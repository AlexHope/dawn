// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "LightDetection/LightDetector.h"
#include "CharacterController/PlayerInteractionController.h"
#include "Character/CrouchCameraController.h"
#include "TP_ThirdPersonCharacter.generated.h"

UCLASS(config = Game)
class ATP_ThirdPersonCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	ATP_ThirdPersonCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	//Light Movement Denial

	/** Amount of Radial Branches to do*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Movement Limitation", meta = (ClampMin = "6", ClampMax = "360"))
	int RadialBranchCount = 12;

	/** Distance between Branch Ray Marches*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Movement Limitation", meta = (ClampMin = "0.1", ClampMax = "10000.0"))
	float BranchRayMarchDistance = 20.0f;

	/** Distance the Ray Marches start from the player*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Movement Limitation", meta = (ClampMin = "0.1", ClampMax = "10000.0"))
	float InitialRaycastDistanceFromPlayer = 30.0f;
	
	/** Max Ray March Count*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Movement Limitation", meta = (ClampMin = "1", ClampMax = "10"))
	int MaxRayMarchCount = 5;

	/** How many consecutive valid ray casts along a Raymarch are required for it to be considered a valid movement choice. Must be less than or equal to 'Max Ray March Count'*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Movement Limitation", meta = (ClampMin = "1", ClampMax = "10"))
	int MarchValidityThreshold = 1;

	/** Visualise the Rays being cast*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Movement Limitation")
	bool bDrawDebuggingLines = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "")
	float MovementDirection = 0;

protected:

	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

private:
	// Wrappers for Crouch and UnCrouch
	void CrouchWrapper();

	void UnCrouchWrapper();

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Light Movement Limitation")
	bool bIsInLight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Movement Limitation")
	bool bRunMovementDenial = true;

	bool bIsMovingLargeObject = false;

protected:

	struct FRayMarchResults
	{
		FVector Vector;
		TArray<bool> Results;
	};

	struct FLightMovementLimitation
	{
		FVector LowerThreshold;
		FVector UpperThreshold;
	};

	TArray<FLightMovementLimitation> MovementLimitationCache;
	TArray<ALightVolume*> ActiveVolumes;
	TArray<ALightVolume*> ActiveLights;
	float PlayerStandingHeadHeight;

	bool bIsInLightVolume;
	bool bIsCrouchWrapperActive;
	bool bIsForcedCrouched;

	const int PlayerZOffset = 20;

	//When we enter the light
	UFUNCTION()
	void OnLightEnter(ALightVolume* LightVolume);

	//When we are in the light
	UFUNCTION()
	void OnLightHit(TArray<FLightHitResult> LightHitResults);

	//When we exit the light
	UFUNCTION()
	void OnLightExit(ALightVolume* LightVolume);

	UFUNCTION()
	void OnLightVolumeEnter(ALightVolume* LightVolume);

	UFUNCTION()
	void OnLightVolumeExit(ALightVolume* LightVolume);

	//Check if we can move against the Cache
	bool IsMovingOutOfLight(FVector Direction);

	//Check if we're within the Volume
	bool IsInVolume(FVector Origin, FVector Extents, FVector Point);

	//Generate the movement limitation graph
	void GenerateMovementLimitation(TArray<FLightHitResult> LightHitResults);

	//Check if the head is in the light
	bool IsHeadInLight(TArray<ALightVolume*> LightHitResults, FVector PlayerHeadPosition);

//public:
//	bool bMediumObjectCanCarry;

private:
	void SetCanCarry();

	UCrouchCameraController* CrouchCameraController;
};