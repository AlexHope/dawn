// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Dawn.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "TP_ThirdPersonCharacter.h"

//////////////////////////////////////////////////////////////////////////
// ATP_ThirdPersonCharacter

ATP_ThirdPersonCharacter::ATP_ThirdPersonCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

												// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

												   // Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
												   // are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

void ATP_ThirdPersonCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Set up our event system
	ULightDetector* LightDetector = FindComponentByClass<ULightDetector>();
	if (LightDetector)
	{
		LightDetector->OnLightEnter.AddDynamic(this, &ATP_ThirdPersonCharacter::OnLightEnter);
		LightDetector->OnLightExit.AddDynamic(this, &ATP_ThirdPersonCharacter::OnLightExit);
		LightDetector->OnLightHitVerbose.AddDynamic(this, &ATP_ThirdPersonCharacter::OnLightHit);
		LightDetector->OnLightVolumeEnter.AddDynamic(this, &ATP_ThirdPersonCharacter::OnLightVolumeEnter);
		LightDetector->OnLightVolumeExit.AddDynamic(this, &ATP_ThirdPersonCharacter::OnLightVolumeExit);
	}

	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = true;

	CrouchCameraController = FindComponentByClass<UCrouchCameraController>();
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATP_ThirdPersonCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATP_ThirdPersonCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATP_ThirdPersonCharacter::MoveRight);

	PlayerInputComponent->BindAction("Crawl", IE_Pressed, this, &ATP_ThirdPersonCharacter::CrouchWrapper);
	PlayerInputComponent->BindAction("Crawl", IE_Released, this, &ATP_ThirdPersonCharacter::UnCrouchWrapper);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATP_ThirdPersonCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATP_ThirdPersonCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ATP_ThirdPersonCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ATP_ThirdPersonCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ATP_ThirdPersonCharacter::OnResetVR);
}

// Called every frame
void ATP_ThirdPersonCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Update our head position
	if (!bIsCrouchWrapperActive && !bIsCrouched && GetMesh() != nullptr && GetMesh()->GetBodyInstance("head") != nullptr)
	{
		PlayerStandingHeadHeight = GetMesh()->GetBodyInstance("head")->GetUnrealWorldTransform().GetLocation().Z;
	}

	//See if we need to crouch
	if (ActiveVolumes.Num() > 0)
	{
		FVector PlayerStandingHeadPos = FVector(GetActorLocation().X, GetActorLocation().Y, PlayerStandingHeadHeight);
		if (IsHeadInLight(ActiveVolumes, PlayerStandingHeadPos))
		{
			bIsForcedCrouched = true;
			//UE_LOG(LogTemp, Warning, TEXT("Forcing Crouch"));
			CrouchWrapper();
		}
		else if (bIsForcedCrouched)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Forcing Uncrouch"));
			UnCrouchWrapper();
		}
	}
	
	if ((ActiveVolumes.Num() <= 0 || ActiveLights.Num() <= 0) && bIsInLight)
	{
		UE_LOG(LogTemp, Warning, TEXT("Light Exit"));
		bIsInLight = false;
	}
}

void ATP_ThirdPersonCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ATP_ThirdPersonCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void ATP_ThirdPersonCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void ATP_ThirdPersonCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATP_ThirdPersonCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATP_ThirdPersonCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		if (!bIsInLight || IsMovingOutOfLight(Direction * FMath::Sign(Value)))
		{
			if (bIsMovingLargeObject)
			{
				AddMovementInput(this->GetActorForwardVector(), Value);
			}
			else
			{
				// add movement in that direction
				AddMovementInput(Direction, Value);

				// TODO: Comment this -- Liam
				//GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, FString(FString::FromInt(Value))); // <-- DEBUG ONLY, DELETE THIS WHEN DONE
				//MovementDirection = Value;
			}
		}
	}
}

void ATP_ThirdPersonCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		if (!bIsInLight || IsMovingOutOfLight(Direction * FMath::Sign(Value)))
		{
			if (!bIsMovingLargeObject)
			{
				// add movement in that direction
				AddMovementInput(Direction, Value);
			}
		}
	}
}

void ATP_ThirdPersonCharacter::CrouchWrapper()
{
	UPlayerInteractionController* InteractionController = FindComponentByClass<UPlayerInteractionController>();
	
	if (!InteractionController)
		return;

	if (InteractionController->LargeObjectCarrying)
	{
		// If player is carrying a large object, end interaction
		InteractionController->InteractKeyReleased();
	}
	else if (InteractionController->MediumObjectCarrying)
	{
		// Else if player is carrying a medium object, end interaction -- OLD
		/*if (UMediumInteractableObject* MediumObject = InteractionController->MediumObjectCarrying->FindComponentByClass<UMediumInteractableObject>())
		{
			InteractionController->ReleaseMediumObject(MediumObject, InteractionController->MediumObjectCarrying->GetActorLocation());
		}*/

		// Else if player is carrying a medium object, do not allow them to crouch
		return;
	}

	InteractionController->bMediumObjectCanCarry = false;
	bIsCrouchWrapperActive = true;
	if (CrouchCameraController)
		CrouchCameraController->bIsCrouching = true;
	ACharacter::Crouch();
}

void ATP_ThirdPersonCharacter::UnCrouchWrapper()
{
	bIsCrouchWrapperActive = false;
	bIsForcedCrouched = false;
	if (CrouchCameraController)
		CrouchCameraController->bIsCrouching = false;
	ACharacter::UnCrouch();

	// Sets a timer that counts 0.1 seconds (I think?) and then runs the method SetCanCarry.
	// Allows for a delay between the player uncrouching and being able to pick up medium objects,
	// as the player is otherwise able to pick up medium objects partway through the transition between
	// crouching and standing, which can lead to medium objects being picked up in awkward ways.
	FTimerHandle SimpleHandle;
	float WaitTime = 0.20f; // Tweak this to adjust the time between uncrouching and being able to pick up medium objects
	GetWorldTimerManager().SetTimer(SimpleHandle, this, &ATP_ThirdPersonCharacter::SetCanCarry, WaitTime, false);
}

void ATP_ThirdPersonCharacter::OnLightEnter(ALightVolume * LightVolume)
{
	//Mark we're in the light
	if (!ActiveLights.Contains(LightVolume))
	{
		UE_LOG(LogTemp, Warning, TEXT("LightEnter"));
		ActiveLights.Add(LightVolume);
	}

	bIsInLight = true;
}

void ATP_ThirdPersonCharacter::OnLightHit(TArray<FLightHitResult> LightHitResults)
{
	bool bFeetInLight = false;
	bool bKneesInLight = false;
	bool bTorsoInLight = false;
	bool bHeadInLight = false;

	//Check which body parts are in the light
	for (int i = 0; i < LightHitResults.Num(); i++)
	{
		if (LightHitResults[i].bInLightHead)
		{
			bHeadInLight = true;
		}

		if (LightHitResults[i].bInLightTorso)
		{
			bTorsoInLight = true;
		}

		if (LightHitResults[i].bInLightKnee)
		{
			bKneesInLight = true;
		}

		if (LightHitResults[i].bInLightFeet || LightHitResults[i].bInLightKnee)
		{
			bFeetInLight = true;
		}
	}

	//If all the body parts are not in light, make sure we tell the character
	if (!bHeadInLight && !bTorsoInLight && !bKneesInLight && !bFeetInLight && bIsInLight)
	{
		UE_LOG(LogTemp, Warning, TEXT("Light Exit"));
		ActiveLights.Empty();
		bIsInLight = false;
	}

	if ((bFeetInLight && !bIsCrouched) || (bIsCrouched && (bKneesInLight || bTorsoInLight)))
	{
		GenerateMovementLimitation(LightHitResults);
	}
}

void ATP_ThirdPersonCharacter::GenerateMovementLimitation(TArray<FLightHitResult> LightHitResults)
{
	//Get player's floor position
	FVector PlayerFloorLocation = GetMesh()->GetComponentLocation();
	PlayerFloorLocation.Z += PlayerZOffset;

	//Create Direction Vectors from the Player for the Ray March
	float MaxDistance = BranchRayMarchDistance * MaxRayMarchCount;
	FVector RayMarchVector = FVector(MaxDistance, 0.0f, 0.0f);
	float AngleBetweenBranches = 360.0f / RadialBranchCount;
	TArray<FRayMarchResults> RadialVectors;
	for (int i = 0; i < 360; i += AngleBetweenBranches)
	{
		FRayMarchResults result;
		result.Vector = RayMarchVector;
		RadialVectors.Add(result);
		RayMarchVector = RayMarchVector.RotateAngleAxis(AngleBetweenBranches, FVector(0.0f, 0.0f, 1.0f));
	}

	//Ray cast along to light source
	FHitResult* HitResult = new FHitResult();
	for (int i = 0; i < RadialBranchCount; i++)
	{
		for (float j = InitialRaycastDistanceFromPlayer; j < MaxDistance; j += BranchRayMarchDistance)
		{
			//The position to raycast from
			FVector RayMarchPos = PlayerFloorLocation + (RadialVectors[i].Vector * (j / MaxDistance));

			bool bIsPointInAnyLight = false;

			for (int k = 0; k < LightHitResults.Num(); k++)
			{
				FVector LightPos = LightHitResults[k].LightVolume->SourceCollider->GetComponentLocation();

				//Is the RayMarchPos within the Volume?
				FVector VolumeOrigin = FVector::ZeroVector;
				FVector VolumeExtents = FVector::ZeroVector;
				LightHitResults[k].LightVolume->GetActorBounds(false, VolumeOrigin, VolumeExtents);
				bool bIsInThisLight = false;

				//If it isnt we consider that a safe area
				if (!IsInVolume(VolumeOrigin, VolumeExtents, RayMarchPos))
				{
#if WITH_EDITOR
					if (GetWorld()->WorldType != EWorldType::Game && bDrawDebuggingLines)
					{
						DrawDebugSphere(GetWorld(), RayMarchPos, 5.0f, 24, FColor(0, 255, 0), false, 3.0f, 0, 1.0f);
					}
#endif
					bIsInThisLight = false;
				}
				else
				{
					//Raycast to the Light source
					GetWorld()->LineTraceSingleByChannel(*HitResult, RayMarchPos, LightPos, ECC_Visibility);

					//If we hit anything that isnt the source collider, then the light is obstructed
					bool bLightObstructed = HitResult->GetComponent() != LightHitResults[k].LightVolume->SourceCollider;

					//Debugging
#if WITH_EDITOR
					if (GetWorld()->WorldType != EWorldType::Game && bDrawDebuggingLines)
					{
						DrawDebugSphere(GetWorld(), RayMarchPos, 5.0f, 24, bLightObstructed ? FColor(0, 255, 0) : FColor(255, 0, 0), false, 3.0f, 0, 1.0f);
					}
#endif
					bIsInThisLight = !bLightObstructed;
				}

				if (bIsInThisLight)
				{
					bIsPointInAnyLight = true;
					break;
				}
			}
			//Add the result
			RadialVectors[i].Results.Add(!bIsPointInAnyLight);
		}
	}

	//Create allowed movement angles
	TArray<FLightMovementLimitation> LimitationGroup;

	//Get the Threshold limit
	int Threshold = (MarchValidityThreshold <= MaxRayMarchCount) ? MarchValidityThreshold : MaxRayMarchCount;

	FVector LowerThreshold = FVector::ZeroVector;
	FVector UpperThreshold = FVector::ZeroVector;

	//Add the first element at the end again for complete circular checks
	if (RadialVectors.Num() >= 0)
	{
		FRayMarchResults copy = RadialVectors[0];
		RadialVectors.Add(copy);
	}

	//Go through all the radial vector's ray marches
	for (int i = 0; i < RadialVectors.Num(); i++)
	{
		FRayMarchResults RayMarchResult = RadialVectors[i];

		int MaxValidConsecutivePoints = 0;
		int ValidConsecutivePoints = 0;

		//Check if it has a consecutive line of raymarches within our threshold
		for (int j = 0; j < RayMarchResult.Results.Num(); j++)
		{
			//Keep count of the consecutive safe areas found
			if (!RayMarchResult.Results[j] || j == RayMarchResult.Results.Num() - 1)
			{
				if (ValidConsecutivePoints > MaxValidConsecutivePoints)
				{
					MaxValidConsecutivePoints = ValidConsecutivePoints;
				}
				ValidConsecutivePoints = 0;
			}
			else
			{
				ValidConsecutivePoints++;
			}
		}

		//If we at any point reach or exceed the max to be valid, Then add this to the limitations
		if (MaxValidConsecutivePoints >= Threshold)
		{
			RayMarchResult.Vector.Normalize();
			if (LowerThreshold == FVector::ZeroVector)
			{
				LowerThreshold = RayMarchResult.Vector;
			}
			else if (UpperThreshold == FVector::ZeroVector)
			{
				UpperThreshold = RayMarchResult.Vector;

				//Make the Limitation Information
				FLightMovementLimitation Limitation;
				Limitation.LowerThreshold = LowerThreshold;
				Limitation.UpperThreshold = UpperThreshold;

				//Add it to our group
				LimitationGroup.Add(Limitation);

				//Reset the thresholds
				LowerThreshold = RayMarchResult.Vector;
				UpperThreshold = FVector::ZeroVector;
			}
		}
		else
		{
			LowerThreshold = FVector::ZeroVector;
			UpperThreshold = FVector::ZeroVector;
		}
	}

#if WITH_EDITOR
	if (GetWorld()->WorldType != EWorldType::Game && bDrawDebuggingLines)
	{
		//Visualise the Limitations
		for (int i = 0; i < LimitationGroup.Num(); i++)
		{
			//Debugging
			FVector DebugLowerThreshold = LimitationGroup[i].LowerThreshold;
			FVector DebugUpperThreshold = LimitationGroup[i].UpperThreshold;

			TArray<FVector> DebugVerts;
			FVector Vert1 = PlayerFloorLocation;
			FVector Vert2 = PlayerFloorLocation + (DebugLowerThreshold * MaxDistance);
			FVector Vert3 = PlayerFloorLocation + (DebugUpperThreshold * MaxDistance);
			DebugVerts.Add(Vert1);
			DebugVerts.Add(Vert2);
			DebugVerts.Add(Vert3);

			TArray<int32> DebugIndices;
			DebugIndices.Add(0);
			DebugIndices.Add(1);
			DebugIndices.Add(2);

			DrawDebugMesh(GetWorld(), DebugVerts, DebugIndices, FColor::Green, false, 3.0f);
		}
	}
#endif

	//Apply it to our cache
	MovementLimitationCache = LimitationGroup;
}

void ATP_ThirdPersonCharacter::OnLightExit(ALightVolume* LightVolume)
{

	if (LightVolume != nullptr /*&& ActiveLights.Contains(LightVolume)*/)
	{
		ActiveLights.Remove(LightVolume);
	}
	else
	{
		ActiveLights.Empty();
	}

	if (ActiveLights.Num() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Light Exit"));
		bIsInLight = false;
	}
}

bool ATP_ThirdPersonCharacter::IsMovingOutOfLight(FVector Direction)
{
	if (!bIsInLight || MovementLimitationCache.Num() <= 0)
	{
		return true;
	}

	//Check all the current limitations
	for (int i = 0; i < MovementLimitationCache.Num(); i++)
	{
		FVector LowerLimit = MovementLimitationCache[i].LowerThreshold;
		FVector UpperLimit = MovementLimitationCache[i].UpperThreshold;

		//Check if the rotation direction to get from the Lower to Upper is the same as the direction from Lower to Direction
		//And the direction from Upper to Lower is the same as Upper to Direction
		if (FVector::DotProduct(FVector::CrossProduct(LowerLimit, Direction), FVector::CrossProduct(LowerLimit, UpperLimit)) >= 0 &&
			FVector::DotProduct(FVector::CrossProduct(UpperLimit, Direction), FVector::CrossProduct(UpperLimit, LowerLimit)) >= 0)
		{
			return true;
		}
	}
	
	return false;
}

bool ATP_ThirdPersonCharacter::IsInVolume(FVector Origin, FVector Extents, FVector Point)
{
	FVector VolumeMax = FVector(Origin.X + Extents.X, Origin.Y + Extents.Y, Origin.Z + Extents.Z);
	FVector VolumeMin = FVector(Origin.X - Extents.X, Origin.Y - Extents.Y, Origin.Z - Extents.Z);

	bool bIsInVolume = !(Point.X < VolumeMin.X || Point.Y < VolumeMin.Y || Point.Z < VolumeMin.Z ||
						 Point.X > VolumeMax.X || Point.Y > VolumeMax.Y || Point.Z > VolumeMax.Z);

	return bIsInVolume;
}

bool ATP_ThirdPersonCharacter::IsHeadInLight(TArray<ALightVolume*> LightHitResults, FVector PlayerHeadPosition)
{
	//Ray cast along to light source
	bool bHeadInAnyLight = false;

	for (int i = 0; i < LightHitResults.Num(); i++)
	{
		FVector LightPos = LightHitResults[i]->SourceCollider->GetComponentLocation();
		FHitResult* HitResult = new FHitResult();

		//Raycast to the Light source
		GetWorld()->LineTraceSingleByChannel(*HitResult, PlayerHeadPosition, LightPos, ECC_Visibility);

		bool bIsInThisLight = (HitResult->GetComponent() == LightHitResults[i]->SourceCollider);

#if WITH_EDITOR
		if (GetWorld()->WorldType != EWorldType::Game && bDrawDebuggingLines)
		{
			DrawDebugSphere(GetWorld(), PlayerHeadPosition, 5.0f, 24, bIsInLight ? FColor(0, 255, 0) : FColor(255, 0, 0), false, 3.0f, 0, 1.0f);
		}
#endif

		if (bIsInThisLight)
		{
			bHeadInAnyLight = true;
			break;
		}
	}

	//If we hit anything that isnt the source collider, then the light is obstructed
	return bHeadInAnyLight;
}


void ATP_ThirdPersonCharacter::OnLightVolumeEnter(ALightVolume* LightVolume)
{
	ActiveVolumes.Add(LightVolume);
}


void ATP_ThirdPersonCharacter::OnLightVolumeExit(ALightVolume* LightVolume)
{
	ActiveVolumes.Remove(LightVolume);
}

void ATP_ThirdPersonCharacter::SetCanCarry()
{
	UPlayerInteractionController* InteractionController = FindComponentByClass<UPlayerInteractionController>();

	if (!InteractionController)
		return;

	InteractionController->bMediumObjectCanCarry = true;
	// GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Cyan, TEXT("SetCanCarry executed"));
}