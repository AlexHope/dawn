// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "WallOcclusionReceiver.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DAWN_API UWallOcclusionReceiver : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWallOcclusionReceiver();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	void RevealPlayer(FVector Location, float Radius);

	UMaterialInstanceDynamic* MaterialInstanceDynamic;

	UPROPERTY(EditAnywhere, Category = "Wall Occlusion Details")
	float RadiusLerpThreshold = 0.01f;

	UPROPERTY(EditAnywhere, Category = "Wall Occlusion Details")
	float RadiusLerpScale = 1.0f;
	
private:

	float Radius = 0.0f;
	bool bIsRevealingPlayer = false;
};
