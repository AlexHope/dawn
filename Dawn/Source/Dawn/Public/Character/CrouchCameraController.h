// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CrouchCameraController.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UCrouchCameraController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCrouchCameraController();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	// Contains references to any Crouch Volumes that the player is currently colliding with
	UPROPERTY(VisibleAnywhere, Category = "CrouchCameraController")
	TArray<AActor*> CrouchVolumeArray;

	// (Accessed from ThirdPersonCharacter)
	bool bIsCrouching;

private:
	// Called when the player crouches while inside a crouch volume
	void TransitionIn();

	// Called when the player uncrouches while inside a crouch volume
	void TransitionOut();

	// Reference to ThirdPersonCharacter's camera boom
	USpringArmComponent* CameraBoom;

	// Reference to ThirdPersonCharacter's skeletal mesh
	USkeletalMeshComponent* Mesh;
	
	// The default length of the camera boom
	float DefaultBoomLength;

	// The speed at which the camera transitions between first and third person
	// CHANGE THIS IF YOU WANT TO CHANGE THE TRANSITION SPEED
	float TransitionSpeed = 8.0f;
};
