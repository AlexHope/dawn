// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "AmbientEffects.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DAWN_API UAmbientEffects : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAmbientEffects();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	// Contains the list of sound cues and their weightings (Integers only)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Effects")
	TMap<USoundCue*, int> AmbientEffects;

	// Minimum time span before another sound can be played (Default: 30)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Effects")
	float MinimumTimeBetweenEffects = 30.0f;

	// Maximum time span before another sound can be played (Default: 60)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound Effects")
	float MaximumTimeBetweenEffects = 60.0f;

private:
	UAudioComponent* AmbientEffectsAudioComponent;

	TArray<int> Weightings;
	TArray<USoundCue*> SoundEffects;

	float RemainingTimeBeforeNextEffect = 60;
	int TotalWeightings = 0;
	bool bIsSoundPlaying = false;
};
