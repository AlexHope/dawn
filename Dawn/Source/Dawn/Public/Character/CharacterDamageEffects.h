// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CharacterDamageEffects.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DAWN_API UCharacterDamageEffects : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCharacterDamageEffects();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, Category = "Character Effects")
	void OnEnterSunlight();

	UFUNCTION(BlueprintCallable, Category = "Character Effects")
	void OnExitSunlight();

	// Time the effects take to fade in (Default = 1.0)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Effects", meta = (DisplayName = "Effect Fade In Time"))
	float AlphaFadeInTimer = 1.0f;

	// Time the effects take to fade out (Default = 1.0)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Effects", meta = (DisplayName = "Effect Fade Out Time"))
	float AlphaFadeOutTimer = 1.0f;

	// Radius of the blur effect (Default = 0.5)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Effects|Blur Effect Settings", meta = (DisplayName = "Blur Radius"))
	float BlurRadius = 0.5f;

	// Exponent of the blur radius (Default = 2.0)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Effects|Blur Effect Settings", meta = (DisplayName = "Blur Radius Exponent"))
	float BlurRadiusExponent = 2.0f;

	// Minimum size of the red edges (Default = 0.60)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Effects|Screen Edge Colour Settings", meta = (DisplayName = "Red Effect Radius Minimum"))
	float RedRadiusMinimum = 0.60f;

	// Maximum size of the red edges (Default = 0.65)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Effects|Screen Edge Colour Settings", meta = (DisplayName = "Red Effect Radius Maximum"))
	float RedRadiusMaximum = 0.65f;

	// 'Hardness' of the red edges (Default = 5.0)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Effects|Screen Edge Colour Settings", meta = (DisplayName = "Red Effect Blend Scale"))
	float RedBlendScale = 5.0f;

	// Time the pulse effect takes to run one full cycle (Default = 2.0)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Effects|Screen Edge Colour Settings", meta = (DisplayName = "Effect Pulse Cycle Time"))
	float PulseCycleTime = 2.0f;

protected:
	class UMaterial* BlurBaseMaterial;
	class UMaterial* RednessBaseMaterial;
	class UMaterialInstanceDynamic* BlurDynamicMaterialInstance;
	class UMaterialInstanceDynamic* RednessDynamicMaterialInstance;

	APlayerController* PlayerController;
	ACharacter* PlayerCharacter;
	APlayerCameraManager* PlayerCamera;

	float TimeInSunlight = 0.0f;
	float Alpha = 0.0f;
	float RedRadiusCurrent = 0.60f;
	float RedRadiusPreviousFrame = 0.0f;

	bool bIsInSunlight = false;

	void FadeEffect(float DeltaTime);
};
