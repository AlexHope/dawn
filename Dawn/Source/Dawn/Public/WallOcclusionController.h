// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "WallOcclusionController.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DAWN_API UWallOcclusionController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWallOcclusionController();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	APlayerCameraManager* PlayerCameraManager;

	UPROPERTY(EditAnywhere, Category = "Wall Occlusion", meta = (ClampMin = "0.0"))
	float Radius = 500.0f;
	
};
