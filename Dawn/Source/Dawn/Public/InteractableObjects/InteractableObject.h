// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InteractableObject.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractableObject : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class DAWN_API IInteractableObject
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION()
	virtual bool OnBeginInteract(AActor* InteractingActor);

	UFUNCTION()
	virtual bool OnEndInteract(AActor* InteractingActor);

	UFUNCTION()
	virtual UShapeComponent* GetInteractionCollider();
};
