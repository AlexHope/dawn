#pragma once

#include "InteractableObject.h"
#include "Components/ActorComponent.h"
#include "MediumInteractableObject.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DAWN_API UMediumInteractableObject : public UActorComponent, public IInteractableObject
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMediumInteractableObject();
	
	// Interface function - Runs when the character first interacts with the object
	UFUNCTION()
	virtual bool OnBeginInteract(AActor* InteractingActor) override;

	// Interface function - Runs when the character ends interaction with the object
	UFUNCTION()
	virtual bool OnEndInteract(AActor* InteractingActor) override;

	// Interface function - Returns the collider of this object
	UFUNCTION()
	virtual UShapeComponent* GetInteractionCollider() override;

	// Used to determine when the player can interact with the object
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object")
	UBoxComponent* InteractionCollider;

	// Following 3 variables used for attach the object to the player
	USkeletalMeshComponent* PlayerSkeletalMesh;
	FName PlayerSocketName;
	FVector PlayerSkeletalMeshRightVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object")
	bool bIsObjectActive;

private:
	bool bIsAttached = false;
};
