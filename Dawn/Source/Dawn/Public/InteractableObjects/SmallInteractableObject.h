// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InteractableObject.h"
#include "Components/ActorComponent.h"
#include <algorithm>
#include <BehaviorTree/BlackboardComponent.h>
#include "SmallInteractableObject.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DAWN_API USmallInteractableObject : public UActorComponent, public IInteractableObject
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USmallInteractableObject();

	// The Interaction Collider
	UPROPERTY(EditAnywhere, Category = "Interactable Object")
	USphereComponent* InteractionCollider;

	// Unique identifier for this object - must not match another object
	UPROPERTY(EditAnywhere, Category = "Interactable Object|Small Object Properties")
	FName ObjectName;

	// Interface function - Runs when the character first interacts with the object
	UFUNCTION()
	virtual bool OnBeginInteract(AActor* InteractingActor) override;

	// Interface function - Runs when the character ends interaction with the object
	UFUNCTION()
	virtual bool OnEndInteract(AActor* InteractingActor) override;

	// Interface function - Returns the collider of this object
	UFUNCTION()
	virtual UShapeComponent* GetInteractionCollider() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object")
	bool bIsObjectActive;
};
