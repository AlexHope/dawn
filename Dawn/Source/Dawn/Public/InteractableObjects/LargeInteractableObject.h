// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InteractableObject.h"
#include "Components/ActorComponent.h"
#include "Classes/Components/SplineComponent.h"
#include "DawnGameInstance.h"
#include "LargeInteractableObject.generated.h"

class ATP_ThirdPersonCharacter;

UENUM(BlueprintType)
enum class EMovementInteraction : uint8
{
	NONE		UMETA(DisplayName = "None"),
	PUSH		UMETA(DisplayName = "Push"),
	PULL		UMETA(DisplayName = "Pull"),
	PUSHPULL	UMETA(DisplayName = "Push & Pull")
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class DAWN_API ULargeInteractableObject : public UActorComponent, public IInteractableObject
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	ULargeInteractableObject();

	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Called when the character begins overlapping with the interaction collider
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Called when the character stops overlapping with the interaction collider
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Interface function - Runs when the character first interacts with the object
	UFUNCTION()
	virtual bool OnBeginInteract(AActor* InteractingActor) override;

	// Interface function - Runs when the character ends interaction with the object
	UFUNCTION()
	virtual bool OnEndInteract(AActor* InteractingActor) override;

	// Interface function - Returns the collider of this object
	UFUNCTION()
	virtual UShapeComponent* GetInteractionCollider() override;

	// Used to determine when the player can interact with the object
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object")
	UBoxComponent* InteractionCollider;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object|Large Object Properties")
	USoundCue* MovementSound;

	// Blocks the Player from colliding with the Mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object")
	UBoxComponent* PlayerBlockerCollider;

	// How the object can be moved
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object|Large Object Properties")
	EMovementInteraction MovementInteraction = EMovementInteraction::PUSHPULL;

	// Prevents the object from moving once it reaches the end of its path
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object|Large Object Properties")
	bool bLockAtEndOfPath = false;

	// Maximum speed the player can move at while moving the object (Default = 50)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object|Large Object Properties")
	float PlayerMaxSpeedWhileMovingLargeObject = 50.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object")
	bool bIsObjectActive;

private:
	void MoveAlongPath(float Distance);
	void OrientPlayerToObject();

	APlayerController* PlayerController;
	ACharacter* PlayerCharacter;
	ATP_ThirdPersonCharacter* ThirdPersonCharacter;

	UDawnGameInstance* DawnGameInstance;

	USplineComponent* Path;
	USceneComponent* Parent;
	UCharacterMovementComponent* PlayerMovement;
	UStaticMeshComponent* StaticMeshComponent;
	UAudioComponent* AudioComponent;

	FVector PlayerLocation;
	FVector PlayerDirection;
	FVector RightVector;
	FVector ForwardVector;
	FVector CurrentDirection;
	FVector PreviousLocation;

	bool bIsInInteractRange = false;
	bool bIsInteracting = false;
	bool bIsMovementOriented = true;
	bool bHasSplineAttached = false;
	bool bIsMoving = false;
	bool bIsSoundPlaying = false;

	float DistanceAlongPath = 0.0f;
	float DefaultWalkSpeed = -1.0f;
};
