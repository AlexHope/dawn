// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "RoomVolumeTrigger.h"
#include "LightDetector.h"
#include "LightVolume.h"
#include "InteractableObjectPlacementGhost.generated.h"

//class PlayerInteractionController;

UCLASS()
class DAWN_API AInteractableObjectPlacementGhost : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AInteractableObjectPlacementGhost();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void UpdateStaticMesh(UStaticMesh* NewMesh);

	UPROPERTY(VisibleAnywhere, Category = "InteractableObjectPlacementGhost")
	bool bIsPlacementMode = false;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void BeginPlacementMode(AActor* MediumObject, FName PlayerSocketName); // THINK OF A BETTER NAME -- Liam

	UFUNCTION()
	FVector EndPlacementMode(); // THINK OF A BETTER NAME -- Liam

	// Called whenever the Player Mesh reference is to be set
	void SetPlayerMesh(USkeletalMeshComponent* Mesh);

	UPROPERTY(VisibleAnywhere, Category = "InteractableObjectPlacementGhost")
	TArray<AActor*> CollidingObjects;

	UFUNCTION()
	bool GetCanPlace();

	void UpdateObjectPosition();

private:
	// Static Mesh Component of this actor
	UPROPERTY(VisibleAnywhere, Category = "InteractableObjectPlacementGhost")
	UStaticMeshComponent* StaticMesh;

	// Interaction Component of this actor
	UPROPERTY(VisibleAnywhere, Category = "InteractableObjectPlacementGhost")
	UBoxComponent* InteractionCollider;

	void UpdateColliderBounds();

	AActor* ActorToGhost;

	// Reference to players skeletal mesh
	USkeletalMeshComponent* PlayerMesh;

	UMaterial* GhostMat;
	UMaterialInstanceDynamic* GhostMatDynamicInstance;

	bool bCanPlace;
};
