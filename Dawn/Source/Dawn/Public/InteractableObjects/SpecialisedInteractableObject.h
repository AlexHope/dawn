// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InteractableObject.h"
#include "Engine.h"
#include "Components/ActorComponent.h"
#include "SpecialisedInteractableObject.generated.h"
//
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBeginInteractEventSignature, class AActor*, InteractingActor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEndInteractEventSignature, class AActor*, InteractingActor);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DAWN_API USpecialisedInteractableObject : public UActorComponent, public IInteractableObject
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USpecialisedInteractableObject();

	// Interface function - Runs when the character first interacts with the object
	UFUNCTION()
	virtual bool OnBeginInteract(AActor* InteractingActor) override;

	// Interface function - Runs when the character ends interaction with the object
	UFUNCTION()
	virtual bool OnEndInteract(AActor* InteractingActor) override;

	// Interface function - Returns the collider of this object
	UFUNCTION()
	virtual UShapeComponent* GetInteractionCollider() override;
	
	// Used to determine when the player can interact with the object
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object")
	UBoxComponent* InteractionCollider;

	/* EVENTS */
	// Triggered when Interacted with
	UPROPERTY(BlueprintAssignable, Category = "Interactable Object")
	FOnBeginInteractEventSignature OnBeginInteractEvent;

	// Triggered when Interaction is over
	UPROPERTY(BlueprintAssignable, Category = "Interactable Object")
	FOnEndInteractEventSignature OnEndInteractEvent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable Object")
	bool bIsObjectActive;

private:
	USceneComponent* Parent;
};
