// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "SmallInteractableObject.h"
#include "MediumInteractableObject.h"
#include "LargeInteractableObject.h"
#include "SpecialisedInteractableObject.h"
#include "PromptUI.h"
#include "InteractableObjectPlacementGhost.h"
#include "CrouchCameraController.h"
#include "PlayerInteractionController.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DAWN_API UPlayerInteractionController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerInteractionController();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	// Set containing reference(s) to the objects the player has acquired
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player Interaction Controller")
	TSet<FName> KeySet;

	// Returns true if the provided key exists in KeySet
	UFUNCTION(BlueprintCallable, Category = "Player Interaction Controller")
	bool KeySetContains(FName Key);

	// Adds provided key to KeySet. Returns true if successful
	UFUNCTION(BlueprintCallable, Category = "Player Interaction Controller")
	bool KeySetAdd(FName Key);

	// Removes provided key from KeySet. Returns true if successful
	UFUNCTION(BlueprintCallable, Category = "Player Interaction Controller")
	bool KeySetRemove(FName Key);

	// Returns bIsInteractingWithobject
	UFUNCTION(BlueprintCallable, Category = "Player Interaction Controller")
	bool GetIsInteractingWithObject();

	void SetIsInteractingWithObject(bool value);

	// Reference to that actor that will be interacted with upon pressing the interact key
	UPROPERTY(BlueprintReadOnly)
	AActor* ActorToInteractWith;
	
	// Reference to the large object being carried
	UPROPERTY(BlueprintReadOnly)
	AActor* LargeObjectCarrying;

	// Reference to the medium object being carried
	UPROPERTY(BlueprintReadOnly)
	AActor* MediumObjectCarrying;

	// Called whenever interaction button is released
	void InteractKeyReleased();

	// Called whenever a medium object needs to be released
	UFUNCTION(BlueprintCallable, Category = "Player Interaction Controller")
	void ReleaseMediumObject(/*UMediumInteractableObject* MediumObject, FVector NewPos*/);

	//
	bool bMediumObjectCanCarry;

	UPROPERTY(BlueprintReadWrite, Category = "Player Interaction Controller")
	bool bHasInteractedWithSmallObject;

	UFUNCTION(BlueprintCallable, Category = "Player Interaction Controller")
	void PickUpSmallObject();

	UFUNCTION(BlueprintCallable, Category = "Player Interaction Controller")
	void PickUpMediumObject();

	UFUNCTION(BlueprintCallable, Category = "Player Interaction Controller")
	void ActivateObjectPlacementGhost();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player Interaction Controller")
	bool bCanPutMediumObjectDown;

private:
	// Reference to skeletal mesh component of parent actor
	USkeletalMeshComponent* SkeletalMesh;

	// Name of the socket to attach interactable objects to
	const FName SocketName = "socket_spine02";

	// Indicates whether this actor is carrying an interactable object or not 
	bool bIsInteractingWithObject = false;

	// Stores references to small objects currently colliding with
	TArray<AActor*> SmallObjectArray;

	// Stores references to medium objects currently colliding with
	TArray<AActor*> MediumObjectArray;

	// Stores references to large objects currently colliding with
	TArray<AActor*> LargeObjectArray;

	// Stores references to specialised objects currently colliding with
	TArray<AActor*> SpecialisedObjectArray;

	// Called whenever the parent actor begins to overlap/collide with an object
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Called whenever the parent actor ends its overlap/collision with an object
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Called whenever the interact button is pressed
	void Interact();

	// Called whenever the player begins or ends an overlap with another object
	void UpdateActorToInteractWith();

	// Medium Object Interaction Ghost (comment this a little better -- Liam)
	AInteractableObjectPlacementGhost* PlacementGhost;

	// Reference to CrouchCameraController component
	UCrouchCameraController* CrouchCameraController;

	//
	FVector NewMediumObjectPosition;

	// Temporary reference to MediumObjectCarrying, used when putting the object down
	AActor* MediumObjectCarryingTemp;
};
