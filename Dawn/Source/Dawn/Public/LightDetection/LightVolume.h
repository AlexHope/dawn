#pragma once

#include "GameFramework/Actor.h"
#include "LightVolume.generated.h"

UCLASS()
class DAWN_API ALightVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALightVolume();

	//The Root Scene Component
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Light Volume")
	USceneComponent* RootSceneComponent;

	//The Light Source Component
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Light Volume")
	UBoxComponent* SourceCollider;

	//The Light Volume Component
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Light Volume")
	UBoxComponent* LightVolumeCollider;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Light Volume")
	bool bInLightLastTest = false;
};
