// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Engine.h"
#include "LightVolume.h"
#include "LightDetector.generated.h"


USTRUCT(BlueprintType)
struct FLightHitResult
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Hit Result")
	ALightVolume* LightVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Hit Result")
	TArray<FBodyInstance> BonesHit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Hit Result")
	bool bInLightHead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Hit Result")
	bool bInLightTorso;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Hit Result")
	bool bInLightKnee;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Light Hit Result")
	bool bInLightFeet;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLightHitSignature, class ALightVolume*, LightVolumeHit, struct FBodyInstance, BoneHit);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLightEnterSignature, class ALightVolume*, LightVolumeEntered);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLightHitVerboseSignature, TArray<FLightHitResult>, HitResults);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLightVolumeEnterSignature, class ALightVolume*, LightVolume);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLightVolumeExitSignature, class ALightVolume*, LightVolume);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLightExitSignature, class ALightVolume*, LightVolumeExited);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class DAWN_API ULightDetector : public UActorComponent
{
	GENERATED_BODY()

public:	
	/* Sets default values for this component's properties */
	ULightDetector();

	/* Called when the game starts */
	virtual void BeginPlay() override;
	
	/* Called every frame */
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;
	
	/* How many times per second to test light detection. (Default = 60.0) */
	UPROPERTY(EditAnywhere, Category = "Light Detection", meta = (DisplayName = "Update Count per Second"))
	unsigned int UpdateCount = 60.0;

	/* Toggle if the detection test should be call upon entering a light volume. (Default = true) */
	UPROPERTY(EditAnywhere, Category = "Light Detection")
	bool bRunOnEnter = true;


	/* EVENTS */
	/* Fired when the detector is in light source. */
	UPROPERTY(BlueprintAssignable, Category = "Light Detection")
	FOnLightHitSignature OnLightHit;

	/* Fired when the detector is first detected in light. */
	UPROPERTY(BlueprintAssignable, Category = "Light Detection")
	FOnLightEnterSignature OnLightEnter;

	/* Fired when the detector exits all light detection */
	UPROPERTY(BlueprintAssignable, Category = "Light Detection")
	FOnLightExitSignature OnLightExit;

	/* Fired when the detector is in the light source.*/
	UPROPERTY(BlueprintAssignable, Category = "Light Detection")
	FOnLightHitVerboseSignature OnLightHitVerbose;

	/* Fired when the detector enters any light volume. */
	UPROPERTY(BlueprintAssignable, Category = "Light Detection")
	FOnLightVolumeEnterSignature OnLightVolumeEnter;

	/* Fired when the detector enters any light volume. */
	UPROPERTY(BlueprintAssignable, Category = "Light Detection")
	FOnLightVolumeEnterSignature OnLightVolumeExit;



	/* Toggle drawing debug lines. Warning this is resource intensive with high refresh rates. */
	UPROPERTY(EditAnywhere, Category = "Light Detection")
	bool bDrawDebuggingLines;

private:
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
	void RunLightTest();
	UFUNCTION()
	void ResetTimer();

	USkeletalMeshComponent* MySkeleton;
	TArray<ALightVolume*>	LightVolumes;	/* Array of light volumes the detector is in. */
	TArray<FBodyInstance*>	Bones;			/* Array of the bones to be used for light detection. */
	float Timer;							/* Timer that controls the rate we run light tests. */
	bool bInLightLastTest;					/* Stores if the character was in light last test. */
};