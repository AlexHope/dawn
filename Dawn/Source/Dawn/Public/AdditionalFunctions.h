// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "AdditionalFunctions.generated.h"

/**
 * 
 */
UCLASS()
class DAWN_API UAdditionalFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Options")
	static TArray<FKey> ReplaceKeybind(TArray<FKey> Keybinds, FKey KeyPressed, FString KeyToReplace, bool &Successful);

	UFUNCTION(BlueprintCallable, Category = "Options")
	static void AssignKeybinds(APlayerController* PlayerController, TArray<FKey> Keybinds, bool bIsInvertedCamera);
};
