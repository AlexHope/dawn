// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/TriggerBox.h"
#include "RoomVolumeTrigger.generated.h"

/**
 * 
 */
UCLASS()
class DAWN_API ARoomVolumeTrigger : public ATriggerBox
{
	GENERATED_BODY()
};
