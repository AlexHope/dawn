// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "PromptUI.generated.h"

/**
 * 
 */
UCLASS()
class DAWN_API UPromptUI : public UUserWidget
{
	GENERATED_BODY()
	
	// https://wiki.unrealengine.com/Extend_UserWidget_for_UMG_Widgets
	
public:
	virtual void NativeConstruct() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom UI Widget")
	FString WidgetName;
};
