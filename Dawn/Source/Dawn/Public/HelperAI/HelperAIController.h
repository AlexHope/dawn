// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "Engine.h"
#include "HelperCharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "HelperAIController.generated.h"

UENUM(BlueprintType)
enum class EAIState : uint8
{
	AIS_IDLE		UMETA(DisplayName = "IDLE"),
	AIS_FOLLOW		UMETA(DisplayName = "FOLLOW"),
	AIS_HELP		UMETA(DisplayName = "HELP"),
	AIS_SEQUENCE	UMETA(DisplayName = "SEQUENCE"),
	AIS_RESET		UMETA(DisplayName = "RESET")
};

UCLASS()
class DAWN_API AHelperAIController : public AAIController
{
	GENERATED_BODY()

public:
	AHelperAIController();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void Possess(APawn* InPawn) override;
	virtual void UnPossess() override;

	UPROPERTY(VisibleAnywhere, Category = "AI")
	EAIState AIState;

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	FName BlackboardPlayerKey;

	UPROPERTY(EditAnywhere, Category = "AI")
	FName BlackboardStateKey;

	UPROPERTY(VisibleAnywhere, Category = "AI")
	ACharacter* PlayerChar;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void AIUpdate();
	bool ChangeAIState(EAIState NewState);
private:
	UBehaviorTreeComponent* BehaviourTree;
	UBlackboardComponent* Blackboard;
	bool bStateLocked;
};
