// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "VolumetricLighting.generated.h"

UCLASS()
class DAWN_API AVolumetricLighting : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVolumetricLighting();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	#if WITH_EDITOR
	// Called when a UPROPERTY is changed
	virtual void PostEditChangeProperty( struct FPropertyChangedEvent& e );
	#endif

	UStaticMeshComponent* FrustumVolume;
	UMaterialInterface* FrustumMaterialInterface;
	UMaterialInstanceDynamic* FrustumDynamicMaterialInstance;

	//Whether the Volumetric Lighting System is attached to the Player Camera, Disable to Disable the effect (Default = true)
	UPROPERTY(EditAnywhere, Category = "Volumetric Lighting")
	bool bActiveOnStart = true;

	//The Colour tint combined with the light's colour for the volumetric light body (Default = (1.0, 1.0, 1.0, 1.0))
	UPROPERTY(EditAnywhere, Category = "Volumetric Lighting")
	FLinearColor TintColour = FLinearColor(1.0f, 1.0f, 1.0f, 1.0f);
	
	//The Opacity of the Volumetric Light, combined with Density (Default = 0.02)
	UPROPERTY(Editanywhere, Category = "Volumetric Lighting", meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float Opacity = 0.02f;
	
	//The Density of the Volumetric Light, combined with Density (Default = 0.2)
	UPROPERTY(Editanywhere, Category = "Volumetric Lighting", meta = (ClampMin = "0.0"))
	float Density = 0.2f;

	//How much it blurs/blends the limits of the Volumetric Light (Default = 1000)
	UPROPERTY(Editanywhere, Category = "Volumetric Lighting", meta = (ClampMin = "0.0"))
	float Blending = 1000.0f;

	//Distance to the camera where it cuts off rendering (Default = 100)
	UPROPERTY(Editanywhere, Category = "Volumetric Lighting", meta = (ClampMin = "0.0"))
	float NearClippingDistance = 100.0f;
	
};
