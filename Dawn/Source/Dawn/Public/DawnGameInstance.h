// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "DawnGameInstance.generated.h"

UCLASS()
class DAWN_API UDawnGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UDawnGameInstance(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options")
	TArray<FKey> CurrentKeybinds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options")
	TArray<FString> CurrentVideoSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options")
	TArray<FString> CurrentUserInterfaceSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options")
	TArray<float> CurrentAudioSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options")
	bool bIsInvertedCamera = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options")
	float MasterVolume = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options")
	float MusicVolume = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options")
	float EffectsVolume = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Options")
	float VoiceVolume = 1.0f;
};
