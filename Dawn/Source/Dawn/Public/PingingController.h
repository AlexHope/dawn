// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "PingingController.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DAWN_API UPingingController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPingingController();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	//Max Alpha of the pinged object (Default = 0.7)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pinging")
	float MaxAlpha = 0.7f;

	//Min Alpha of the pinged object (Default = 0.0)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pinging")
	float MinAlpha = 0.0f;

	//The duration of the Alpha decay (Default = 3.0)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pinging")
	float AlphaDecayDuration = 3.0f;

	//The fill colour (Default = (1.0, 1.0, 1.0, 1.0))
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pinging")
	FLinearColor FillColour = FLinearColor(1.0f, 1.0f, 1.0f, 1.0f);

	//The distance from the camera to allow the effect (Default = 1000.0f)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pinging")
	float DistanceThreshold = 1000.0f;

protected:

	class UMaterial* BaseMaterial;
	class UMaterialInstanceDynamic* DynamicMaterialInstance;

	float Alpha = 0.0f;
	float AlphaDecayTimer = 0.0f;

	/** Called when the user pings the designated object */
	void StartPing();

	/** Decays the Alpha on the Xray PP */
	void DecayPing(float DeltaSeconds);
};