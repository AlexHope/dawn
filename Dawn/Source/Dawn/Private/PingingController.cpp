// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "PingingController.h"


// Sets default values for this component's properties
UPingingController::UPingingController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//Get Material
	BaseMaterial = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/Shaders/PP_Outline.PP_Outline'")).Object;
}


// Called when the game starts
void UPingingController::BeginPlay()
{
	Super::BeginPlay();

	Alpha = MinAlpha;
	AlphaDecayTimer = 0.0f;

	IInterface_PostProcessVolume* InterfacePostProcessVolume;

	if (GetWorld()->PostProcessVolumes.Num() > 0)
	{
		//Get Post Processing Volume
		InterfacePostProcessVolume = *GetOwner()->GetWorld()->PostProcessVolumes.GetData();
	}
	else
	{
		#if WITH_EDITOR
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("ERROR: No Post Processing Volume Found!"));
		}
		#endif
		return;
	}

	if (InterfacePostProcessVolume != nullptr)
	{
		//Get a Material Interface
		UMaterialInterface* MaterialInterface = Cast<UMaterialInterface>(BaseMaterial);

		//Create a Dynamic Instance
		DynamicMaterialInstance = UMaterialInstanceDynamic::Create(MaterialInterface, nullptr, TEXT("MID_Outline"));

		//Add to the Post Process Volume
		APostProcessVolume* PostProcessVolume = Cast<APostProcessVolume>(InterfacePostProcessVolume);
		PostProcessVolume->AddOrUpdateBlendable(DynamicMaterialInstance, 1.0f);

		DynamicMaterialInstance->SetScalarParameterValue(TEXT("Alpha"), Alpha);
		DynamicMaterialInstance->SetVectorParameterValue(TEXT("FillColour"), FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
		DynamicMaterialInstance->SetScalarParameterValue(TEXT("Threshold"), DistanceThreshold);
	}
	
	// Add pinging listener
	GetOwner()->InputComponent->BindAction("Ping", IE_Pressed, this, &UPingingController::StartPing);
}


// Called every frame
void UPingingController::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	//If the Alpha component is active, we'll decay it
	DecayPing(DeltaTime);
}

void UPingingController::StartPing()
{
	//Print debug message
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT("Ping"));
	}

	//Max out the Alpha and set the internal timer
	Alpha = MaxAlpha;
	AlphaDecayTimer = AlphaDecayDuration;
	DynamicMaterialInstance->SetScalarParameterValue(TEXT("Alpha"), Alpha);
}

void UPingingController::DecayPing(float DeltaSeconds)
{
	if ((AlphaDecayTimer -= DeltaSeconds) < 0.0f)
	{
		AlphaDecayTimer = 0.0f;
		Alpha = MinAlpha;
	}
	else
	{
		Alpha = (MaxAlpha - MinAlpha) * (AlphaDecayTimer / AlphaDecayDuration);
	}

	DynamicMaterialInstance->SetScalarParameterValue(TEXT("Alpha"), Alpha);
}

