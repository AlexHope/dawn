// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "VolumetricLighting.h"


// Sets default values
AVolumetricLighting::AVolumetricLighting()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AVolumetricLighting::BeginPlay()
{
	Super::BeginPlay();

	if (bActiveOnStart)
	{
		//Get Frustum
		FrustumVolume = FindComponentByClass<UStaticMeshComponent>();
		FrustumMaterialInterface = FrustumVolume->GetMaterial(0);

		//Create Dynamic Material Instance
		FrustumDynamicMaterialInstance = UMaterialInstanceDynamic::Create(FrustumMaterialInterface, nullptr, TEXT("MID_VolumetricLighting"));

		//Set Default Parameters
		FrustumDynamicMaterialInstance->SetVectorParameterValue("Tint Colour", TintColour);
		FrustumDynamicMaterialInstance->SetScalarParameterValue("Opacity", Opacity);
		FrustumDynamicMaterialInstance->SetScalarParameterValue("Density", Density);
		FrustumDynamicMaterialInstance->SetScalarParameterValue("Blending", Blending);
		FrustumDynamicMaterialInstance->SetScalarParameterValue("Near Clipping Distance", NearClippingDistance);

		//Set the Dynamic Material Instance to the Frustum Material
		FrustumVolume->SetMaterial(0, FrustumDynamicMaterialInstance);

		//Attach Fog Anchor to Player Camera Manager Component
		APlayerCameraManager* PlayerCameraManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
		UBillboardComponent* Billboard = FindComponentByClass<UBillboardComponent>();

		Billboard->AttachToComponent(PlayerCameraManager->GetTransformComponent(),
			FAttachmentTransformRules::FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, true),
			TEXT("BB_VolumetricLighting"));
	}
	
}

#if WITH_EDITOR
void AVolumetricLighting::PostEditChangeProperty(struct FPropertyChangedEvent& e)
{
	//Find which property has changed
	FName PropertyName = (e.Property != NULL) ? e.Property->GetFName() : NAME_None;
	
	//If the Tint Colour has been changed
	if (FrustumDynamicMaterialInstance)
	{
		//Tint Colour
		if (PropertyName == GET_MEMBER_NAME_CHECKED(AVolumetricLighting, TintColour))
		{
			//Tell the Dynamic Material Instance
			FrustumDynamicMaterialInstance->SetVectorParameterValue("Tint Colour", TintColour);
		}
		//Opacity
		else if (PropertyName == GET_MEMBER_NAME_CHECKED(AVolumetricLighting, Opacity))
		{
			//Tell the Dynamic Material Instance
			FrustumDynamicMaterialInstance->SetScalarParameterValue("Opacity", Opacity);
		}
		//Density
		else if (PropertyName == GET_MEMBER_NAME_CHECKED(AVolumetricLighting, Density))
		{
			//Tell the Dynamic Material Instance
			FrustumDynamicMaterialInstance->SetScalarParameterValue("Density", Density);
		}
		//Near Clipping Distance
		else if (PropertyName == GET_MEMBER_NAME_CHECKED(AVolumetricLighting, NearClippingDistance))
		{
			//Tell the Dynamic Material Instance
			FrustumDynamicMaterialInstance->SetScalarParameterValue("Near Clipping Distance", NearClippingDistance);
		}
		//Shadow Fade Distance
		else if (PropertyName == GET_MEMBER_NAME_CHECKED(AVolumetricLighting, Blending))
		{
			//Tell the Dynamic Material Instance
			FrustumDynamicMaterialInstance->SetScalarParameterValue("Blending", Blending);
		}
	}

	//Super::PostEditChangeProperty(e);
}
#endif

// Called every frame
void AVolumetricLighting::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

