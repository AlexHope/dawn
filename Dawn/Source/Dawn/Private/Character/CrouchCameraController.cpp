// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "CrouchCameraController.h"


// Sets default values for this component's properties
UCrouchCameraController::UCrouchCameraController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UCrouchCameraController::BeginPlay()
{
	Super::BeginPlay();

	CameraBoom = GetOwner()->FindComponentByClass<USpringArmComponent>();
	Mesh = GetOwner()->FindComponentByClass<USkeletalMeshComponent>();
	DefaultBoomLength = CameraBoom->TargetArmLength;
}


// Called every frame
void UCrouchCameraController::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	if (bIsCrouching && CrouchVolumeArray.Num() > 0)
	{
		// Player is crouching inside a Crouch Volume
		TransitionIn();
	}
	else if ((bIsCrouching && CrouchVolumeArray.Num() == 0) || !bIsCrouching)
	{
		// Player is crouching outside a Crouch Volume, or is not crouching
		TransitionOut();
	}
}

void UCrouchCameraController::TransitionIn()
{
	if (CameraBoom->TargetArmLength > 0)
		CameraBoom->TargetArmLength -= TransitionSpeed;

	if (CameraBoom->TargetArmLength < 50 && Mesh->IsVisible())
		Mesh->SetVisibility(false);
}

void UCrouchCameraController::TransitionOut()
{
	if (CameraBoom->TargetArmLength < DefaultBoomLength)
		CameraBoom->TargetArmLength += TransitionSpeed;

	if (CameraBoom->TargetArmLength <= 50 && !Mesh->IsVisible())
		Mesh->SetVisibility(true);
}