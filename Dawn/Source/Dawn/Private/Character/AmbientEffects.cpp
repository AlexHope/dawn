// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "AmbientEffects.h"


// Sets default values for this component's properties
UAmbientEffects::UAmbientEffects()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Create the audio component that will play the sound effect
	AmbientEffectsAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AmbientEffectsAudioComponent"));
}


// Called when the game starts
void UAmbientEffects::BeginPlay()
{
	Super::BeginPlay();

	AmbientEffects.GenerateValueArray(Weightings);
	AmbientEffects.GenerateKeyArray(SoundEffects);

	if (MinimumTimeBetweenEffects > MaximumTimeBetweenEffects || MinimumTimeBetweenEffects <= 0 || MaximumTimeBetweenEffects <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("AmbientEffects.cpp - Warning: MinimumTimeBetweenEffects and/or MaximumTimeBetweenEffects set to incompatible value(s). Defaulting MinimumTimeBetweenEffects to 30 & MaximumTimeBetweenEffects to 60."))
		MinimumTimeBetweenEffects = 30.0f;
		MaximumTimeBetweenEffects = 60.0f;
	}

	RemainingTimeBeforeNextEffect = FMath::RandRange(MinimumTimeBetweenEffects, MaximumTimeBetweenEffects);

	for (int i = 0; i < Weightings.Num(); i++)
	{
		TotalWeightings += Weightings[i];
	}
}


// Called every frame
void UAmbientEffects::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	RemainingTimeBeforeNextEffect -= DeltaTime;

	if (RemainingTimeBeforeNextEffect < 0 && !bIsSoundPlaying)
	{
		int SoundSeed = FMath::RandRange(0, TotalWeightings);

		for (int i = 0; i < Weightings.Num(); i++)
		{
			if (SoundSeed <= Weightings[i])
			{
				AmbientEffectsAudioComponent->Sound = SoundEffects[i];
				break;
			}

			SoundSeed -= Weightings[i];
		}

		AmbientEffectsAudioComponent->Play();
		RemainingTimeBeforeNextEffect = FMath::RandRange(MinimumTimeBetweenEffects, MaximumTimeBetweenEffects);
	}
	else if (RemainingTimeBeforeNextEffect < 0 && bIsSoundPlaying)
	{
		RemainingTimeBeforeNextEffect += 10;
	}

	if (AmbientEffectsAudioComponent->IsPlaying())
	{
		bIsSoundPlaying = true;
	}
	else
	{
		bIsSoundPlaying = false;
	}
}

