// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "Engine.h"
#include "CharacterDamageEffects.h"


// Sets default values for this component's properties
UCharacterDamageEffects::UCharacterDamageEffects()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//Get Material
	BlurBaseMaterial = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/Shaders/PP_Blur.PP_Blur'")).Object;
	RednessBaseMaterial = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/Shaders/PP_Redness.PP_Redness'")).Object;
}


// Called when the game starts
void UCharacterDamageEffects::BeginPlay()
{
	Super::BeginPlay();

	// Get useful player statics
	PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	PlayerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	PlayerCamera = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);

	IInterface_PostProcessVolume* InterfacePostProcessVolume;
	if (GetWorld()->PostProcessVolumes.Num() > 0)
	{
		//Get Post Processing Volume
		InterfacePostProcessVolume = *GetOwner()->GetWorld()->PostProcessVolumes.GetData();
	}
	else
	{
		#if WITH_EDITOR
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("ERROR: No Post Processing Volume Found!"));
		}
		#endif
		return;
	}

	if (InterfacePostProcessVolume != nullptr)
	{
		//Get a Material Interface
		UMaterialInterface* BlurMaterialInterface = Cast<UMaterialInterface>(BlurBaseMaterial);
		UMaterialInterface* RednessMaterialInterface = Cast<UMaterialInterface>(RednessBaseMaterial);

		//Create a Dynamic Instance
		BlurDynamicMaterialInstance = UMaterialInstanceDynamic::Create(BlurMaterialInterface, nullptr, TEXT("MID_Blur"));
		RednessDynamicMaterialInstance = UMaterialInstanceDynamic::Create(RednessMaterialInterface, nullptr, TEXT("MID_Redness"));

		//Add to the Post Process Volume
		APostProcessVolume* PostProcessVolume = Cast<APostProcessVolume>(InterfacePostProcessVolume);
		PostProcessVolume->AddOrUpdateBlendable(BlurDynamicMaterialInstance, 1.0f);
		PostProcessVolume->AddOrUpdateBlendable(RednessDynamicMaterialInstance, 1.0f);

		BlurDynamicMaterialInstance->SetScalarParameterValue(TEXT("BlurAmount"), 0.0f);
		RednessDynamicMaterialInstance->SetScalarParameterValue(TEXT("RedAlpha"), 0.0f);
		RednessDynamicMaterialInstance->SetScalarParameterValue(TEXT("RedRadius"), RedRadiusMaximum);
	}
	BlurDynamicMaterialInstance->SetScalarParameterValue(TEXT("BlurRadius"), BlurRadius);
	BlurDynamicMaterialInstance->SetScalarParameterValue(TEXT("BlurRadiusExponent"), BlurRadiusExponent);
	RednessDynamicMaterialInstance->SetScalarParameterValue(TEXT("RedBlendScale"), RedBlendScale);
}


// Called every frame
void UCharacterDamageEffects::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	FadeEffect(DeltaTime);
}

void UCharacterDamageEffects::OnEnterSunlight()
{
	bIsInSunlight = true;
}

void UCharacterDamageEffects::OnExitSunlight()
{
	bIsInSunlight = false;
}

void UCharacterDamageEffects::FadeEffect(float DeltaTime)
{
	if (bIsInSunlight)
	{
		if (TimeInSunlight + DeltaTime < AlphaFadeInTimer)
		{
			TimeInSunlight += DeltaTime;
			Alpha += DeltaTime / AlphaFadeInTimer;
			BlurDynamicMaterialInstance->SetScalarParameterValue(TEXT("BlurAmount"), Alpha);
			RednessDynamicMaterialInstance->SetScalarParameterValue(TEXT("RedAlpha"), Alpha);
		}

		float RadiusChange = (2 * (RedRadiusMaximum - RedRadiusMinimum)) * (DeltaTime / PulseCycleTime);

		if (RedRadiusCurrent <= RedRadiusMinimum)
			RedRadiusCurrent = RedRadiusMinimum + RadiusChange;
		else if (RedRadiusCurrent >= RedRadiusMaximum)
			RedRadiusCurrent = RedRadiusMaximum - RadiusChange;

		if (RedRadiusCurrent > RedRadiusPreviousFrame)
		{
			RedRadiusPreviousFrame = RedRadiusCurrent;
			RedRadiusCurrent += RadiusChange;
		}
		else
		{
			RedRadiusPreviousFrame = RedRadiusCurrent;
			RedRadiusCurrent -= RadiusChange;
		}

		RednessDynamicMaterialInstance->SetScalarParameterValue(TEXT("RedRadius"), RedRadiusCurrent);
	}
	else if (!bIsInSunlight && TimeInSunlight - DeltaTime > 0.0f)
	{
		TimeInSunlight -= DeltaTime;
		Alpha -= DeltaTime / AlphaFadeOutTimer;
		BlurDynamicMaterialInstance->SetScalarParameterValue(TEXT("BlurAmount"), Alpha);
		RednessDynamicMaterialInstance->SetScalarParameterValue(TEXT("RedAlpha"), Alpha);
	}
}
