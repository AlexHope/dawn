// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "Engine.h"
#include "TP_ThirdPerson/TP_ThirdPersonCharacter.h"
#include "LargeInteractableObject.h"

// Sets default values for this component's properties
ULargeInteractableObject::ULargeInteractableObject()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Create a box collider component used for detecting whether the player is in range to interact
	InteractionCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractionCollider"));

	// Create the audio component to handle movement sounds
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio"));

	// Create a box collider component that blocks only the player to avoid tunneling when moving the object along a spline
	// - Tunneling causes the sweep result of MoveAlongPath() to return false something, causing the object to get 'stuck'
	PlayerBlockerCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("PlayerBlockerCollider"));
	PlayerBlockerCollider->SetCollisionResponseToAllChannels(ECR_Ignore);
	PlayerBlockerCollider->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	PlayerBlockerCollider->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	PlayerBlockerCollider->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);

	if (GetOwner())
	{
		// Get the parent component and attach the colliders to it
		Parent = GetOwner()->GetRootComponent();
		InteractionCollider->SetupAttachment(Parent);
		PlayerBlockerCollider->SetupAttachment(Parent);

		// Grab the world size of the parent in units
		FVector Scale, Origin;
		GetOwner()->GetActorBounds(false, Origin, Scale);

		// Set the size of the colliders to be relative to the size of the parent
		// The 'magic numbers' added on for the X and Y scale only set the default values when the component is added
		// Can be manually rescaled inside of the editor if required
		InteractionCollider->SetBoxExtent(FVector(Scale.X + 10, Scale.Y + 10, Scale.Z), true);
		PlayerBlockerCollider->SetBoxExtent(FVector(Scale.X + 2, Scale.Y + 2, Scale.Z + 0.1f), true);
	}

	// Add delegates that run when something enters/exits the interaction collider
	InteractionCollider->OnComponentBeginOverlap.AddDynamic(this, &ULargeInteractableObject::OnOverlapBegin);
	InteractionCollider->OnComponentEndOverlap.AddDynamic(this, &ULargeInteractableObject::OnOverlapEnd);

	//
	if (GetOwner())
	{
		GetOwner()->Tags.Empty();
		GetOwner()->Tags.Add(FName("InteractableObject"));
		GetOwner()->Tags.Add(FName("LargeInteractableObject"));
	}

	bIsObjectActive = true;
}


// Called when the game starts
void ULargeInteractableObject::BeginPlay()
{
	Super::BeginPlay();

	// Get the game instance
	DawnGameInstance = Cast<UDawnGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	// Get useful player statics
	PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	PlayerCharacter = PlayerController->GetCharacter();
	if (PlayerCharacter)
	{
		PlayerMovement = PlayerCharacter->FindComponentByClass<UCharacterMovementComponent>();
		ThirdPersonCharacter = Cast<ATP_ThirdPersonCharacter>(PlayerCharacter);
	}

	bIsMovementOriented = true;
	ForwardVector = Parent->GetForwardVector();
	RightVector = Parent->GetRightVector();
	PreviousLocation = GetOwner()->GetActorLocation();

	AudioComponent->Sound = MovementSound;

	// Grab the static mesh component if it exists (it should)
	if (GetOwner()->FindComponentByClass<UStaticMeshComponent>())
	{
		StaticMeshComponent = GetOwner()->FindComponentByClass<UStaticMeshComponent>();

		if (MovementInteraction != EMovementInteraction::NONE)
		{
			StaticMeshComponent->Mobility = EComponentMobility::Movable;
		}
	}

	// Make sure there's a spline on the actor if it has a movement interaction and is Large
	if (MovementInteraction != EMovementInteraction::NONE)
	{
		if (GetOwner()->FindComponentByClass<USplineComponent>())
		{
			// Detach the spline from the parent to ensure that it doesn't move with the object when the object moves
			Path = GetOwner()->FindComponentByClass<USplineComponent>();
			Path->DetachFromComponent(FDetachmentTransformRules(EDetachmentRule::KeepWorld, false));

			CurrentDirection = Path->GetWorldLocationAtSplinePoint(1) - GetOwner()->GetActorLocation();
			CurrentDirection.Normalize();

			bHasSplineAttached = true;
		}
	}

	#if WITH_EDITOR
	// Debug - Displays splines (remove when done testing)
	PlayerController->ConsoleCommand("ShowFlag.Splines 1", true);
	#endif
}


// Called every frame
void ULargeInteractableObject::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bIsMoving && !bIsSoundPlaying)
	{
		AudioComponent->Play();	
		bIsSoundPlaying = true;
	}
	else if (!bIsMoving && bIsSoundPlaying)
	{
		AudioComponent->Stop();
		bIsSoundPlaying = false;
	}

	// Check if the player is in the interaction range of a Large object and is holding the interaction key
	if (bIsInInteractRange && bIsInteracting)
	{
		// Retrieve the normal vector from the player to the object
		PlayerLocation = PlayerCharacter->GetActorLocation();
		PlayerDirection = GetOwner()->GetActorLocation() - PlayerLocation;
		PlayerDirection.Z = 0;
		PlayerDirection.Normalize();
		OrientPlayerToObject();

		// Modify this if statement if inputs ever changed for movement (determines whether or not the player is moving forwards)
		if ((MovementInteraction == EMovementInteraction::PUSH || MovementInteraction == EMovementInteraction::PUSHPULL) && (PlayerController->IsInputKeyDown(DawnGameInstance->CurrentKeybinds[0]) || PlayerController->IsInputKeyDown(EKeys::Gamepad_LeftStick_Up)))
		{
			MoveAlongPath(PlayerMaxSpeedWhileMovingLargeObject * DeltaTime);
		}
		else if ((MovementInteraction == EMovementInteraction::PULL || MovementInteraction == EMovementInteraction::PUSHPULL) && (PlayerController->IsInputKeyDown(DawnGameInstance->CurrentKeybinds[1]) || PlayerController->IsInputKeyDown(EKeys::Gamepad_LeftStick_Down)))
		{
			MoveAlongPath(-PlayerMaxSpeedWhileMovingLargeObject * DeltaTime);
		}
	}

	if (PreviousLocation != GetOwner()->GetActorLocation())
	{
		bIsMoving = true;
		PreviousLocation = GetOwner()->GetActorLocation();
	}
	else
	{
		bIsMoving = false;
	}
}

void ULargeInteractableObject::MoveAlongPath(float Distance)
{
	if (bHasSplineAttached)
	{
		// Ensure we don't go off the end of the spline
		if (DistanceAlongPath < 0)
		{
			DistanceAlongPath = 0;
		}
		else if (DistanceAlongPath > Path->GetSplineLength())
		{
			if (bLockAtEndOfPath)
			{
				DistanceAlongPath = Path->GetSplineLength();
			}
			else
			{
				// If the object reaches the end of the spline it locks position, so we push it slightly back off the end to prevent this
				DistanceAlongPath = Path->GetSplineLength() - 0.01f;
			}
		}

		// Change the player's maximum movement speed whilst moving the object and disable character orientation
		PlayerMovement->MaxWalkSpeed = PlayerMaxSpeedWhileMovingLargeObject;

		// Check if the player is parallel to the spline direction (in the positive direction)
		// & Moves the object along the path
		// - Returns false if the object would collide with something
		if (FVector::DotProduct(PlayerDirection, CurrentDirection) > 0 && GetOwner()->SetActorLocation(Path->GetWorldLocationAtDistanceAlongSpline(DistanceAlongPath + Distance), true))
		{
			DistanceAlongPath += Distance;

			if (Distance > 0)
			{
				ThirdPersonCharacter->MovementDirection = 1.0f;
			}
			else
			{
				ThirdPersonCharacter->MovementDirection = -1.0f;
			}
		}
		// Check if the player is parallel to the spline direction (in the negative direction)
		// & Moves the object along the path
		// - Returns false if the object would collide with something
		else if (FVector::DotProduct(PlayerDirection, CurrentDirection) < 0 && GetOwner()->SetActorLocation(Path->GetWorldLocationAtDistanceAlongSpline(DistanceAlongPath - Distance), true))
		{
			DistanceAlongPath -= Distance;

			if (Distance > 0)
			{
				ThirdPersonCharacter->MovementDirection = 1.0f;
			}
			else
			{
				ThirdPersonCharacter->MovementDirection = -1.0f;
			}
		}

		CurrentDirection = Path->GetDirectionAtDistanceAlongSpline(DistanceAlongPath, ESplineCoordinateSpace::World);
	}
}

void ULargeInteractableObject::OrientPlayerToObject()
{
	// Check if the player is parallel to the spline direction (in the positive direction)
	if (FVector::DotProduct(PlayerDirection, CurrentDirection) > 0)
	{
		// Orient the player to face the side of the object they are pushing/pulling from
		bIsMovementOriented = PlayerMovement->bOrientRotationToMovement = false;
		PlayerCharacter->SetActorRotation(FRotator(CurrentDirection.ToOrientationRotator()));
	}
	// Check if the player is parallel to the spline direction (in the negative direction)
	else if (FVector::DotProduct(PlayerDirection, CurrentDirection) < 0)
	{
		// Orient the player to face the side of the object they are pushing/pulling from
		bIsMovementOriented = PlayerMovement->bOrientRotationToMovement = false;
		PlayerCharacter->SetActorRotation(FRotator((CurrentDirection * -1).ToOrientationRotator()));
	}
}

// Delegate function - runs when the player overlaps with the interaction collider
void ULargeInteractableObject::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == PlayerCharacter)
	{
		bIsInInteractRange = true;
	}
}

// Delegate function - runs when the player overlaps with the interaction collider
void ULargeInteractableObject::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == PlayerCharacter && bIsInteracting)
	{
		bIsInInteractRange = false;

		OnEndInteract(OtherActor);	
	}	
}

bool ULargeInteractableObject::OnBeginInteract(AActor* InteractingActor)
{
	if (bIsInInteractRange)
	{
		bIsInteracting = true;
		DefaultWalkSpeed = PlayerMovement->MaxWalkSpeed;
		ThirdPersonCharacter->bIsMovingLargeObject = true;
	}

	// Ignore this, does nothing (don't remove though)
	return true;
}

bool ULargeInteractableObject::OnEndInteract(AActor* InteractingActor)
{
	bIsInteracting = false;
	ThirdPersonCharacter->FindComponentByClass<UPlayerInteractionController>()->SetIsInteractingWithObject(false);
	if (DefaultWalkSpeed != -1.0f) 
		PlayerMovement->MaxWalkSpeed = DefaultWalkSpeed;
	bIsMovementOriented = PlayerMovement->bOrientRotationToMovement = true;
	ThirdPersonCharacter->bIsMovingLargeObject = false;

	if (bIsSoundPlaying)
	{
		AudioComponent->Stop();
		bIsSoundPlaying = false;
		bIsMoving = false;
	}

	// Ignore this, does nothing (don't remove though)
	return true;
}

UShapeComponent* ULargeInteractableObject::GetInteractionCollider()
{
	return InteractionCollider;
}

