// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "SpecialisedInteractableObject.h"

// Sets default values for this component's properties
USpecialisedInteractableObject::USpecialisedInteractableObject()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Create a box collider component used for detecting whether the player is in range to interact
	InteractionCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractionCollider"));

	if (GetOwner())
	{
		// Get the parent component and attach the colliders to it
		Parent = GetOwner()->GetRootComponent();
		InteractionCollider->SetupAttachment(Parent);

		// Grab the world size of the parent in units
		FVector Scale, Origin;
		GetOwner()->GetActorBounds(false, Origin, Scale);

		// Set the size of the colliders to be relative to the size of the parent
		// The 'magic numbers' added on for the X and Y scale only set the default values when the component is added
		// Can be manually changed inside of the editor if required
		InteractionCollider->SetBoxExtent(FVector(Scale.X + 10, Scale.Y + 10, Scale.Z), true);

		GetOwner()->Tags.Empty();
		GetOwner()->Tags.Add(FName("InteractableObject"));
		GetOwner()->Tags.Add(FName("SpecialisedInteractableObject"));
	}

	bIsObjectActive = true;
}

bool USpecialisedInteractableObject::OnBeginInteract(AActor* InteractingActor)
{
	OnBeginInteractEvent.Broadcast(InteractingActor);

	// Ignore this, does nothing (don't remove though)
	return true;
}

bool USpecialisedInteractableObject::OnEndInteract(AActor* InteractingActor)
{
	OnEndInteractEvent.Broadcast(InteractingActor);

	// Ignore this, does nothing (don't remove though)
	return true;
}

UShapeComponent* USpecialisedInteractableObject::GetInteractionCollider()
{
	return InteractionCollider;
}
