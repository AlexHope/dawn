// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "InteractableObjectPlacementGhost.h"
#include "PressurePlateComp.h"


// Sets default values
AInteractableObjectPlacementGhost::AInteractableObjectPlacementGhost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(GetRootComponent());

	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/Geometry/Meshes/1M_Cube"));

	if (CubeVisualAsset.Succeeded())
	{
		StaticMesh->SetStaticMesh(CubeVisualAsset.Object);
		StaticMesh->SetWorldScale3D(FVector(0.5));
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> Mat_Green(TEXT("/Game/ThirdPersonCPP/InteractionGhostMaterial"));

	if (Mat_Green.Succeeded())
	{
		GhostMat = Mat_Green.Object;
	}

	FVector Min, Max;
	StaticMesh->GetLocalBounds(Min, Max);

	InteractionCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractionCollider"));	
	InteractionCollider->SetupAttachment(StaticMesh);
	InteractionCollider->SetBoxExtent(FVector(Max.X + 5, Max.Y + 5, Max.Z + 5));
	InteractionCollider->SetNotifyRigidBodyCollision(true); // https://answers.unrealengine.com/questions/127168/simulation-generate-hit-events-boolean-in-c.html

	StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	bCanPlace = false;
}

// Called when the game starts or when spawned
void AInteractableObjectPlacementGhost::BeginPlay()
{
	Super::BeginPlay();

	InteractionCollider->OnComponentBeginOverlap.AddDynamic(this, &AInteractableObjectPlacementGhost::OnOverlapBegin);
	InteractionCollider->OnComponentEndOverlap.AddDynamic(this, &AInteractableObjectPlacementGhost::OnOverlapEnd);

	UMaterialInterface* MaterialInterface = Cast<UMaterialInterface>(GhostMat);
	GhostMatDynamicInstance = UMaterialInstanceDynamic::Create(MaterialInterface, nullptr, TEXT("GreenMat"));
	StaticMesh->SetMaterial(0, GhostMatDynamicInstance);
}

// Called every frame
void AInteractableObjectPlacementGhost::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	//check(GhostMatDynamicInstance);
	//UE_LOG(LogTemp, Warning, TEXT("MID Name: %s"), *GhostMatDynamicInstance->GetName());

	if (bIsPlacementMode)
	{
		// TODO: Give reference to target object, set own position to target plus offset to place object at floor.
		//       If colliding with something, turn red and disallow object dropping. Else, stay green and allow
		//       object dropping.

		if (CollidingObjects.Num() > 0)
		{
			// Red
			GhostMatDynamicInstance->SetVectorParameterValue("ColourOverlay", FLinearColor(1.0, 0.0, 0.0));
			bCanPlace = false;
		}
		else
		{
			// Green
			GhostMatDynamicInstance->SetVectorParameterValue("ColourOverlay", FLinearColor(0.0, 1.0, 0.0));
			bCanPlace = true;
		}
	}
}

void AInteractableObjectPlacementGhost::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ATriggerBase* Blah = Cast<ATriggerBase>(OtherActor))
		return;

	if (ARoomVolumeTrigger* Blah = Cast<ARoomVolumeTrigger>(OtherActor))
		return;

	if (ALightVolume* Blah = Cast<ALightVolume>(OtherActor))
		return;

	if (OtherActor->FindComponentByClass<ULightDetector>())
		return;

	if (OtherActor->FindComponentByClass<UPressurePlateComp>())
		return;

	if (OtherActor == ActorToGhost)
		return;

	CollidingObjects.Add(OtherActor);
}

void AInteractableObjectPlacementGhost::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (CollidingObjects.Contains(OtherActor))
	{
		CollidingObjects.Remove(OtherActor);
	}
	else
	{
		// TODO: Something Else
	}
}

void AInteractableObjectPlacementGhost::SetPlayerMesh(USkeletalMeshComponent* Mesh)
{
	PlayerMesh = Mesh;
}

void AInteractableObjectPlacementGhost::BeginPlacementMode(AActor* MediumObject, FName PlayerSocketName)
{
	ActorToGhost = MediumObject;
	bIsPlacementMode = true;

	StaticMesh->AttachToComponent(PlayerMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, true), PlayerSocketName);

	SetActorScale3D(MediumObject->GetActorScale());
	FRotator Rotation(0, ActorToGhost->GetActorRotation().Yaw, ActorToGhost->GetActorRotation().Roll);
	SetActorRotation(Rotation);
	float ForwardOffset = 60.0f;
	float DownOffset = 48.0f;
	FVector NewLocation = FVector((PlayerMesh->GetRightVector() * ForwardOffset) + (FVector(0, 0, -1) * DownOffset) + PlayerMesh->GetOwner()->GetActorLocation());
	SetActorLocation(NewLocation);

	//TArray<UTexture*> UsedTextures;
	//ActorToGhost->FindComponentByClass<UStaticMeshComponent>()->GetStaticMesh()->GetMaterial(0)->GetUsedTextures(UsedTextures, EMaterialQualityLevel::High, true, ERHIFeatureLevel::SM5, true);
	//GhostMatDynamicInstance->SetTextureParameterValue("ParentBaseMaterial", UsedTextures[0]);
	GhostMatDynamicInstance->SetVectorParameterValue("ColourOverlay", FLinearColor(0.0, 1.0, 0.0));
	StaticMesh->SetMaterial(0, GhostMatDynamicInstance);
}

FVector AInteractableObjectPlacementGhost::EndPlacementMode()
{
	StaticMesh->DetachFromComponent(FDetachmentTransformRules(EDetachmentRule::KeepWorld, false));
	ActorToGhost = nullptr;
	bIsPlacementMode = false;
	FVector ReturnPos = GetActorLocation();
	FVector NewLocation = FVector(0, 0, 0);
	SetActorLocation(NewLocation);
	return ReturnPos;
}

void AInteractableObjectPlacementGhost::UpdateStaticMesh(UStaticMesh* NewMesh)
{
	StaticMesh->SetStaticMesh(NewMesh);
	UpdateColliderBounds();
}

void AInteractableObjectPlacementGhost::UpdateColliderBounds()
{
	FVector Min, Max;
	StaticMesh->GetLocalBounds(Min, Max);
	InteractionCollider->SetBoxExtent(FVector(Max.X + 5, Max.Y + 5, Max.Z + 5));
}

bool AInteractableObjectPlacementGhost::GetCanPlace()
{
	return bCanPlace;
}

void AInteractableObjectPlacementGhost::UpdateObjectPosition()
{
	float ForwardOffset = 60.0f;
	float DownOffset = 40.0f;
	FVector NewLocation = FVector((PlayerMesh->GetRightVector() * ForwardOffset) + (FVector(0, 0, -1) * DownOffset) + PlayerMesh->GetOwner()->GetActorLocation());
	SetActorLocation(NewLocation);
}