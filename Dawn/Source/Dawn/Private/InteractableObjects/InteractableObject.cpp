// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "Engine.h"
#include "InteractableObject.h"


// This function does not need to be modified.
UInteractableObject::UInteractableObject(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

bool IInteractableObject::OnBeginInteract(AActor* InteractingActor)
{
	#if WITH_EDITOR
	GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, "DEBUG: Interface function called: OnBeginInteract! Implement override function in class!" );
	#endif

	return true;
}

bool IInteractableObject::OnEndInteract(AActor* InteractingActor)
{
	#if WITH_EDITOR
	GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, "DEBUG: Interface function called: OnEndInteract! Implement override function in class!");
	#endif

	return true;
}

UShapeComponent* IInteractableObject::GetInteractionCollider()
{
	#if WITH_EDITOR
	GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, "DEBUG: Interface function called: GetInteractionCollider! Implement override function in class!");
	#endif

	return nullptr;
}