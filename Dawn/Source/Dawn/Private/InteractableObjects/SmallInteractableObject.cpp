// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "SmallInteractableObject.h"

// Sets default values for this component's properties
USmallInteractableObject::USmallInteractableObject()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// TODO:
	// Set sphere radius to max axis size of actor bounds plus a reasonable amount
	// Attach sphere collider to parent object

	InteractionCollider = CreateDefaultSubobject<USphereComponent>(TEXT("InteractionCollider"));

	if (GetOwner())
	{
		FVector Scale, Origin;
		GetOwner()->GetActorBounds(false, Origin, Scale);

		InteractionCollider->SetupAttachment(GetOwner()->GetRootComponent());
		InteractionCollider->SetSphereRadius(Scale.GetMax() + 100);
	}

	//
	if (GetOwner())
	{
		GetOwner()->Tags.Empty();
		GetOwner()->Tags.Add(FName("InteractableObject"));
		GetOwner()->Tags.Add(FName("SmallInteractableObject"));
	}

	bIsObjectActive = true;
}

bool USmallInteractableObject::OnBeginInteract(AActor* InteractingActor)
{
	// Add any extra functionality here if desired

	// Ignore this, does nothing (don't remove though)
	return true;
}

bool USmallInteractableObject::OnEndInteract(AActor* InteractingActor)
{
	if (GetOwner())
	{
		GetOwner()->Destroy();
	}

	// Ignore this, does nothing (don't remove though)
	return true;
}

UShapeComponent* USmallInteractableObject::GetInteractionCollider()
{
	return InteractionCollider;
}