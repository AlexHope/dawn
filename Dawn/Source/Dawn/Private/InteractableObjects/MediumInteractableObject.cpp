// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "MediumInteractableObject.h"

// Sets default values for this component's properties
UMediumInteractableObject::UMediumInteractableObject()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Create a box collider component used for detecting whether the player is in range to interact
	InteractionCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractionCollider"));

	if (GetOwner())
	{
		InteractionCollider->SetupAttachment(GetOwner()->GetRootComponent());

		FVector Scale, Origin;
		GetOwner()->GetActorBounds(false, Origin, Scale);

		InteractionCollider->SetBoxExtent(FVector(Scale.X + 20, Scale.Y + 20, Scale.Z + 20), true);
	}

	//
	if (GetOwner())
	{
		GetOwner()->Tags.Empty();
		GetOwner()->Tags.Add(FName("InteractableObject"));
	}

	bIsObjectActive = true;
}

bool UMediumInteractableObject::OnBeginInteract(AActor* InteractingActor)
{
	if (!bIsAttached)
	{
		// Attach parent object to supplied scene component at supplied socket
		GetOwner()->AttachToComponent(PlayerSkeletalMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, true), PlayerSocketName);

		// Rotation Stuff
		//FRotator Rot = FRotator(0, InteractingActor->GetActorRotation().Yaw, InteractingActor->GetActorRotation().Roll);
		//InteractingActor->SetActorRotation(Rot);

		// Calculate offset location to be applied to parent actor
		float ForwardOffset = 50.0f;
		float DownOffset = 20.0f;
		FVector NewLocation = ((PlayerSkeletalMeshRightVector * ForwardOffset) + (InteractingActor->GetActorUpVector() * - DownOffset)) + GetOwner()->GetActorLocation();
		// Set parent actor location to offsetted location
		GetOwner()->SetActorLocation(NewLocation, false);
		// Disable collision so parent actor does not collide with player
		GetOwner()->SetActorEnableCollision(false);
		// Disable physics simulation. TODO: Replace this with a pointer eventually for error checking?
		GetOwner()->FindComponentByClass<UStaticMeshComponent>()->SetSimulatePhysics(false);

		bIsAttached = true;
	}

	// Ignore this, does nothing (don't remove though)
	return true;
}

bool UMediumInteractableObject::OnEndInteract(AActor* InteractingActor)
{
	if (bIsAttached)
	{
		// Detach parent actor from the component it is currently attached to
		GetOwner()->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, EDetachmentRule::KeepWorld, EDetachmentRule::KeepWorld, false));
		// Reenable actor collision
		GetOwner()->SetActorEnableCollision(true);
		// Reenable physics simulation. TODO: Replace this with a pointer eventually for error checking?
		GetOwner()->FindComponentByClass<UStaticMeshComponent>()->SetSimulatePhysics(true);

		GetOwner()->SetActorLocation(GetOwner()->GetActorLocation() + FVector(0.0f, 0.0f, 10.0f));
		PlayerSkeletalMesh = nullptr;
		PlayerSocketName = "";
		PlayerSkeletalMeshRightVector = FVector::ZeroVector;
		bIsAttached = false;
	}

	// Ignore this, does nothing (don't remove though)
	return true;
}

UShapeComponent* UMediumInteractableObject::GetInteractionCollider()
{
	return InteractionCollider;
}

//void UMediumInteractableObject::UpdateObjectPosition()
//{
//	// Calculate offset location to be applied to parent actor
//	float Offset = 50.0f;
//	FVector NewLocation = (PlayerSkeletalMesh->GetRightVector() * Offset) + /*GetOwner()->GetActorLocation()*/PlayerSkeletalMesh->GetOwner()->GetActorLocation();
//	// Set parent actor location to offsetted location
//	GetOwner()->SetActorLocation(NewLocation, false);
//}