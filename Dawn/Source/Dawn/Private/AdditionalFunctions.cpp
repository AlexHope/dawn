// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "AdditionalFunctions.h"

TArray<FKey> UAdditionalFunctions::ReplaceKeybind(TArray<FKey> Keybinds, FKey KeyPressed, FString KeyToReplace, bool &Successful)
{
	int IndexOfPreviousKeybind = -1;

	// Keybinds are stored as follows: Forwards=0, Backwards=1, Left=2, Right=3, Crouch=4, Interact=5, Ping=6, Pause Menu=7
	if (KeyToReplace == "ForwardsKey")
	{
		IndexOfPreviousKeybind = 0;
	}
	else if (KeyToReplace == "BackwardsKey")
	{
		IndexOfPreviousKeybind = 1;
	}
	else if (KeyToReplace == "LeftStrafeKey")
	{
		IndexOfPreviousKeybind = 2;
	}
	else if (KeyToReplace == "RightStrafeKey")
	{
		IndexOfPreviousKeybind = 3;
	}
	else if (KeyToReplace == "CrouchKey")
	{
		IndexOfPreviousKeybind = 4;
	}
	else if (KeyToReplace == "InteractKey")
	{
		IndexOfPreviousKeybind = 5;
	}
	else if (KeyToReplace == "PingKey")
	{
		IndexOfPreviousKeybind = 6;
	}
	else if (KeyToReplace == "PauseMenuKey")
	{
		IndexOfPreviousKeybind = 7;
	}

	if (Keybinds.Contains(KeyPressed))
	{
		Successful = false;
		return Keybinds;
	}

	if (IndexOfPreviousKeybind == -1)
	{
		UE_LOG(LogTemp, Warning, TEXT("ERROR: AdditionalFunctions.cpp IndexOfPreviousKeybind is not set correctly!"));
		Successful = false;
		return Keybinds;
	}

	Keybinds.RemoveAt(IndexOfPreviousKeybind);
	Keybinds.Insert(KeyPressed, IndexOfPreviousKeybind);

	Successful = true;
	return Keybinds;
}

void UAdditionalFunctions::AssignKeybinds(APlayerController* PlayerController, TArray<FKey> Keybinds, bool bIsInvertedCamera)
{
	// Ensure no old action inputs are left over
	PlayerController->PlayerInput->ActionMappings.Empty();
	PlayerController->PlayerInput->AxisMappings.Empty();

	/* GAMEPAD CONTROLS */

	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("MoveForward", EKeys::Gamepad_LeftStick_Up, 1.0));
	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("MoveForward", EKeys::Gamepad_LeftStick_Down, -1.0));
	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("MoveRight", EKeys::Gamepad_LeftStick_Right, 1.0));
	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("MoveRight", EKeys::Gamepad_LeftStick_Left, -1.0));

	if (bIsInvertedCamera)
	{
		PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("LookUpRate", EKeys::Gamepad_RightY, 1.0));
	}
	else
	{
		PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("LookUpRate", EKeys::Gamepad_RightY, -1.0));
	}

	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("TurnRate", EKeys::Gamepad_RightX, 1.0));

	PlayerController->PlayerInput->AddActionMapping(FInputActionKeyMapping("Crawl", EKeys::Gamepad_FaceButton_Right, false, false, false, false));
	PlayerController->PlayerInput->AddActionMapping(FInputActionKeyMapping("Interact", EKeys::Gamepad_FaceButton_Left, false, false, false, false));
	PlayerController->PlayerInput->AddActionMapping(FInputActionKeyMapping("PauseMenu", EKeys::Gamepad_Special_Right, false, false, false, false));

	/* MOUSE & KEYBOARD CONTROLS */

	// Mouse
	if (bIsInvertedCamera)
	{
		PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("LookUp", EKeys::MouseY, 1.0));
	}
	else
	{
		PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("LookUp", EKeys::MouseY, -1.0));
	}
	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("Turn", EKeys::MouseX, 1.0));

	// Move Forwards
	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("MoveForward", Keybinds[0], 1.0));

	// Move Backwards
	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("MoveForward", Keybinds[1], -1.0));

	// Move Left
	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("MoveRight", Keybinds[2], -1.0));

	// Move Right
	PlayerController->PlayerInput->AddAxisMapping(FInputAxisKeyMapping("MoveRight", Keybinds[3], 1.0));

	// Crouch
	PlayerController->PlayerInput->AddActionMapping(FInputActionKeyMapping("Crawl", Keybinds[4], false, false, false, false));

	// Interact
	PlayerController->PlayerInput->AddActionMapping(FInputActionKeyMapping("Interact", Keybinds[5], false, false, false, false));

	// Pinging (OBSOLETE - NOT ANYMORE FARTNUGGET)
	PlayerController->PlayerInput->AddActionMapping(FInputActionKeyMapping("Ping", Keybinds[6], false, false, false, false));

	// Open Pause Menu
	PlayerController->PlayerInput->AddActionMapping(FInputActionKeyMapping("PauseMenu", Keybinds[7], false, false, false, false));
}