// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "HelperAIController.h"
#include "RoomVolumeTrigger.h"

AHelperAIController::AHelperAIController()
{
	PrimaryActorTick.bCanEverTick = true;
	BlackboardPlayerKey = "Player";
	BlackboardStateKey = "AIState";
	Blackboard = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard"));
	BehaviourTree = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviourTree"));
}

void AHelperAIController::BeginPlay()
{
	Super::BeginPlay();

	/* Get the player character and set it in the blackboard. */
	PlayerChar = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (PlayerChar)
		Blackboard->SetValueAsObject(BlackboardPlayerKey, PlayerChar);
}

void AHelperAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHelperAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
	if (AHelperCharacter* MyHelperChar = Cast<AHelperCharacter>(InPawn))
	{
		MyHelperChar->GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AHelperAIController::OnBeginOverlap);
		MyHelperChar->GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &AHelperAIController::OnEndOverlap);
		UCapsuleComponent* const Capsule = MyHelperChar->GetCapsuleComponent();
		
		if (MyHelperChar->BehaviourTree->BlackboardAsset)
		{
			Blackboard->InitializeBlackboard(*MyHelperChar->BehaviourTree->BlackboardAsset);

			TArray<AActor*> Actors;
			Capsule->GetOverlappingActors(Actors, ARoomVolumeTrigger::StaticClass());
			if (Actors.Num() > 0)
			{
				Blackboard->SetValueAsObject(FName("CurrentRoom"), Actors[0]);
			}
		}

		if (PlayerChar && Blackboard)
		{
			Blackboard->SetValueAsObject(BlackboardPlayerKey, PlayerChar);
		}
		BehaviourTree->StartTree(*MyHelperChar->BehaviourTree);
	}
}

void AHelperAIController::UnPossess()
{
	Super::UnPossess();
	BehaviourTree->StopTree();
}

void AHelperAIController::AIUpdate()
{

}

bool AHelperAIController::ChangeAIState(EAIState NewState)
{
	AIState = NewState;
	return true;
}

void AHelperAIController::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARoomVolumeTrigger* Room = Cast<ARoomVolumeTrigger>(OtherActor))
	{
		#if WITH_EDITOR
			GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Green, Room->GetHumanReadableName());
		#endif
		Blackboard->SetValueAsObject(FName("CurrentRoom"), Room);
	}
}

void AHelperAIController::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}