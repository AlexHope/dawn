// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "HelperCharacter.h"


// Sets default values
AHelperCharacter::AHelperCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AHelperCharacter::BeginPlay()
{
	Super::BeginPlay();
	PlayerChar = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
}

// Called every frame
void AHelperCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

