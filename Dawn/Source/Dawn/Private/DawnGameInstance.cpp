// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "DawnGameInstance.h"

UDawnGameInstance::UDawnGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	CurrentKeybinds.Add(EKeys::W);
	CurrentKeybinds.Add(EKeys::S);
	CurrentKeybinds.Add(EKeys::A);
	CurrentKeybinds.Add(EKeys::D);
	CurrentKeybinds.Add(EKeys::LeftControl);
	CurrentKeybinds.Add(EKeys::E);
	CurrentKeybinds.Add(EKeys::Q);
	CurrentKeybinds.Add(EKeys::F10);
	
	CurrentUserInterfaceSettings.Add("Off");

	CurrentVideoSettings.Add("16:9");
	CurrentVideoSettings.Add("1280x720");
	CurrentVideoSettings.Add("Windowed");
	CurrentVideoSettings.Add("Off");
	CurrentVideoSettings.Add("Off");

	CurrentAudioSettings.Add(1);
	CurrentAudioSettings.Add(1);
	CurrentAudioSettings.Add(1);
	CurrentAudioSettings.Add(1);
}
