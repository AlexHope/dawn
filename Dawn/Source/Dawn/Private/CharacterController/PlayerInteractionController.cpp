// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "PlayerInteractionController.h"


// Sets default values for this component's properties
UPlayerInteractionController::UPlayerInteractionController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	bIsInteractingWithObject = false;
	MediumObjectCarrying = nullptr;
	LargeObjectCarrying = nullptr;
	bHasInteractedWithSmallObject = false;
	bMediumObjectCanCarry = true;
	bCanPutMediumObjectDown = false;
}

// Called when the game starts
void UPlayerInteractionController::BeginPlay()
{
	Super::BeginPlay();

	// Get capsule collider of parent actor
	if (UCapsuleComponent* CapsuleCollider = GetOwner()->FindComponentByClass<UCapsuleComponent>())
	{
		// Add overlap begin and end listeners to the CapsuleCollider
		CapsuleCollider->OnComponentBeginOverlap.AddDynamic(this, &UPlayerInteractionController::OnOverlapBegin);
		CapsuleCollider->OnComponentEndOverlap.AddDynamic(this, &UPlayerInteractionController::OnOverlapEnd);
	}

	// Get skeletal mesh of parent actor
	SkeletalMesh = GetOwner()->FindComponentByClass<USkeletalMeshComponent>();

	// Add interact listener
	GetOwner()->InputComponent->BindAction("Interact", IE_Pressed, this, &UPlayerInteractionController::Interact);
	GetOwner()->InputComponent->BindAction("Interact", IE_Released, this, &UPlayerInteractionController::InteractKeyReleased);

	// Spawn the medium object placement ghost
	FActorSpawnParameters SpawnParams;
	SpawnParams.Name = "PlacementGhost";
	FVector Location = FVector(0, 0, 0);
	PlacementGhost = GetWorld()->SpawnActor<AInteractableObjectPlacementGhost>(Location, FRotator::ZeroRotator, SpawnParams);

	if (SkeletalMesh)
		PlacementGhost->SetPlayerMesh(SkeletalMesh);
	else
		;// TODO: Error message

	CrouchCameraController = GetOwner()->FindComponentByClass<UCrouchCameraController>();
}


// Called every frame
void UPlayerInteractionController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//check(GhostMatDynamicInstance);
	//if (ActorToInteractWith == nullptr)
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("Actor to Interact With: null"));
	//}
	//else
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("Actor to Interact With: %s"), *ActorToInteractWith->GetName());
	//}
}

bool UPlayerInteractionController::KeySetContains(FName Key)
{
	return KeySet.Contains(Key);
}

bool UPlayerInteractionController::KeySetAdd(FName Key)
{
	bool bAddSuccessful = false;
	KeySet.Add(Key, &bAddSuccessful);
	UE_LOG(LogTemp, Warning, TEXT("Adding Key %s Result %B"), *Key.ToString(), bAddSuccessful)
	return bAddSuccessful;
}

bool UPlayerInteractionController::KeySetRemove(FName Key)
{
	return KeySet.Remove(Key) > 0 ? true : false;
}

void UPlayerInteractionController::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (USmallInteractableObject* SmallObject = OtherActor->FindComponentByClass<USmallInteractableObject>())
	{
		// If supplied object is a SmallInteractableObject,
		// add reference to object to SmallObjectArray
		if (SmallObject->bIsObjectActive)
		{
			SmallObjectArray.Add(OtherActor);
			UpdateActorToInteractWith();
		}
	}
	else if (UMediumInteractableObject* MediumObject = OtherActor->FindComponentByClass<UMediumInteractableObject>())
	{
		// Else if supplied object is a MediumInteractableObject,
		// add reference to object to MediumObjectArray
		if (MediumObject->bIsObjectActive)
		{
			MediumObjectArray.Add(OtherActor);
			UpdateActorToInteractWith();
		}
	}
	else if (ULargeInteractableObject* LargeObject = OtherActor->FindComponentByClass<ULargeInteractableObject>())
	{
		// Else if supplied object is a LargeInteractableObject,
		// add reference to object to LargeObjectArray
		if (LargeObject->bIsObjectActive)
		{
			LargeObjectArray.Add(OtherActor);
			UpdateActorToInteractWith();
		}		
	}
	else if (USpecialisedInteractableObject* SpecialObject = OtherActor->FindComponentByClass<USpecialisedInteractableObject>())
	{
		// Else if supplied object is a SpecialisedInteractableObject,
		// add reference to object to SpecialisedObjectArray
		if (SpecialObject->bIsObjectActive)
		{
			SpecialisedObjectArray.Add(OtherActor);
			UpdateActorToInteractWith();
		}
	}
	else if (ATriggerBox* TriggerBox = Cast<ATriggerBox>(OtherActor))
	{
		if (OtherActor->Tags.Contains("CrouchVolume"))
		{
			if (CrouchCameraController)
				CrouchCameraController->CrouchVolumeArray.Add(OtherActor);
		}
	}
}

void UPlayerInteractionController::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (SmallObjectArray.Contains(OtherActor))
	{
		// If supplied object is a SmallInteractableObject,
		// remove reference to object from SmallObjectArray
		SmallObjectArray.Remove(OtherActor);
		UpdateActorToInteractWith();
	}
	else if (MediumObjectArray.Contains(OtherActor))
	{
		// Else if supplied object is a MediumInteractableObject,
		// remove reference to object from MediumObjectArray
		MediumObjectArray.Remove(OtherActor);
		UpdateActorToInteractWith();
	}
	else if (LargeObjectArray.Contains(OtherActor))
	{
		// Else if supplied object is a LargeInteractableObject,
		// remove reference to object from LargeObjectArray
		LargeObjectArray.Remove(OtherActor);
		UpdateActorToInteractWith();
	}
	else if (SpecialisedObjectArray.Contains(OtherActor))
	{
		// Else if supplied object is a SpecialisedInteractableObject,
		// remove reference to object from SpecialisedObjectArray
		SpecialisedObjectArray.Remove(OtherActor);
		UpdateActorToInteractWith();
	}
	else if (ATriggerBox* TriggerBox = Cast<ATriggerBox>(OtherActor))
	{
		if (OtherActor->Tags.Contains("CrouchVolume"))
		{
			if (CrouchCameraController)
				CrouchCameraController->CrouchVolumeArray.Remove(OtherActor);
		}
	}
}

void UPlayerInteractionController::Interact()
{
	// Check if character is crouching/crawling. If so, do not allow to interact with an object.
	UCharacterMovementComponent* movementComp = GetOwner()->FindComponentByClass<UCharacterMovementComponent>();
	bool bIsPlayerCrouching = !movementComp || movementComp->IsCrouching();
	//if (!movementComp || movementComp->IsCrouching()) 
	//	return;

	if (bIsInteractingWithObject && MediumObjectCarrying != nullptr && bCanPutMediumObjectDown)
	{
		// If the player is carrying an object, then grab the UMediumInteractableObject component from the object.
		// If the component was successfully found, detach the object from the player.	

		if (UMediumInteractableObject* MediumObject = MediumObjectCarrying->FindComponentByClass<UMediumInteractableObject>())
		{
			/*bIsInteractingWithObject = false;
			MediumObjectCarrying = nullptr;
			MediumObject->OnEndInteract(this->GetOwner());*/

			if (PlacementGhost->GetCanPlace())
			{
				// FVector NewPos = PlacementGhost->EndPlacementMode();
				// bIsInteractingWithObject = false;
				NewMediumObjectPosition = PlacementGhost->EndPlacementMode();
				MediumObjectCarryingTemp = MediumObjectCarrying;
				MediumObjectCarrying = nullptr;
				// ReleaseMediumObject(MediumObject, NewPos);
			}
		}
	}
	else
	{
		if (SmallObjectArray.Num() > 0 && !bIsInteractingWithObject)
		{
			// If there are objects stored in SmallObjectArray, grab the first object and interact with it.
			if (USmallInteractableObject* SmallObject = SmallObjectArray[SmallObjectArray.Num() - 1]->FindComponentByClass<USmallInteractableObject>())
			{
				if (bIsPlayerCrouching)
				{
					PickUpSmallObject();
					return;
				}

				bHasInteractedWithSmallObject = true;

				//SmallObject->OnBeginInteract(this->GetOwner());

				//// Check if the player already owns an object by this name
				//bool bIsKeyAlreadyOwned = false;
				//KeySet.Add(SmallObject->ObjectName, &bIsKeyAlreadyOwned);
				//if (bIsKeyAlreadyOwned)
				//{
				//	#if WITH_EDITOR
				//	GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("ERROR: PlayerInteractionController::Object with this key already exists in KeySet."));
				//	#endif
				//	return;
				//}

				//SmallObjectArray.RemoveAt(SmallObjectArray.Num() - 1);

				//// Destroy the object
				//SmallObject->OnEndInteract(this->GetOwner());

				return;
			}
			else
			{
				#if WITH_EDITOR
				GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("ERROR: PlayerInteractionController::Unrecognised class type inside of USmallInteractableObject array! Cleaning array..."));
				#endif
				SmallObjectArray.RemoveAt(SmallObjectArray.Num() - 1);
			}
		}
		else if (MediumObjectArray.Num() > 0 && !bIsPlayerCrouching && !bIsInteractingWithObject)
		{
			// Else if there are objects stored in MediumObjectArray, grab the most recent and interact with it
			if (UMediumInteractableObject* MediumObject = MediumObjectArray[MediumObjectArray.Num() - 1]->FindComponentByClass<UMediumInteractableObject>())
			{
				if (!bMediumObjectCanCarry)
					return;

				bIsInteractingWithObject = true;
				MediumObjectCarrying = MediumObjectArray[MediumObjectArray.Num() - 1];

				/*UStaticMeshComponent* MeshComp = MediumObjectCarrying->FindComponentByClass<UStaticMeshComponent>();
				if (MeshComp)
					PlacementGhost->UpdateStaticMesh(MeshComp->GetStaticMesh());
				else
					GEngine->AddOnScreenDebugMessage(0, 10.0f, FColor::Red, TEXT("PlayerInteractionController::Interact - Medium Object Mesh Component not found."));

				// Tell the object where to attach to
				MediumObject->PlayerSkeletalMesh = SkeletalMesh;
				MediumObject->PlayerSocketName = SocketName;
				MediumObject->PlayerSkeletalMeshRightVector = SkeletalMesh->GetRightVector();

				FRotator NewRotation = SkeletalMesh->GetRightVector().Rotation();
				MediumObjectCarrying->SetActorRotation(NewRotation);
				MediumObject->OnBeginInteract(this->GetOwner());
				PlacementGhost->BeginPlacementMode(MediumObjectCarrying, SocketName);*/
				return;
			}
			else
			{
				#if WITH_EDITOR
				GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("ERROR: PlayerInteractionController::Unrecognised class type inside of UMediumInteractableObject array! Cleaning array..."));
				#endif
				MediumObjectArray.RemoveAt(MediumObjectArray.Num() - 1);
			}
		}
		else if (LargeObjectArray.Num() > 0 && !bIsPlayerCrouching && !bIsInteractingWithObject)
		{
			// Else if there are objects stored in LargeObjectArray, grab the most recent and interact with it.
			if (ULargeInteractableObject* LargeObject = LargeObjectArray[LargeObjectArray.Num() - 1]->FindComponentByClass<ULargeInteractableObject>())
			{
				bIsInteractingWithObject = true;
				LargeObjectCarrying = LargeObjectArray[LargeObjectArray.Num() - 1];
				// TODO: Sort out character animation.
				LargeObject->OnBeginInteract(this->GetOwner());
				return;
			}
			else
			{
				#if WITH_EDITOR
				GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("ERROR: PlayerInteractionController::Unrecognised class type inside of ULargeInteractableObject array! Cleaning array..."));
				#endif
				LargeObjectArray.RemoveAt(LargeObjectArray.Num() - 1);
			}
		}
		else if (SpecialisedObjectArray.Num() > 0 && !bIsPlayerCrouching)
		{
			// Else if there are objects stored in SpecialisedObjectArray, grab the most recent object and interact with it.
			if (USpecialisedInteractableObject* SpecialObject = SpecialisedObjectArray[SpecialisedObjectArray.Num() - 1]->FindComponentByClass<USpecialisedInteractableObject>())
			{
				// TODO: Sort out character animation.
				SpecialObject->OnBeginInteract(this->GetOwner());
				if (SpecialObject->bIsObjectActive == false)
				{
					SpecialisedObjectArray.Remove(SpecialObject->GetOwner());
					UpdateActorToInteractWith();
				}
				return;
			}
			else
			{
				#if WITH_EDITOR
				GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("ERROR: PlayerInteractionController::Unrecognised class type inside of USpecialisedInteractableObject array! Cleaning array..."));
				#endif
				SpecialisedObjectArray.RemoveAt(SpecialisedObjectArray.Num() - 1);
			}
		}
	}
}

bool UPlayerInteractionController::GetIsInteractingWithObject()
{
	return bIsInteractingWithObject;
}

void UPlayerInteractionController::SetIsInteractingWithObject(bool value)
{
	bIsInteractingWithObject = value;
}

void UPlayerInteractionController::UpdateActorToInteractWith()
{
	//If currently interacting, don't update and return
	if (bIsInteractingWithObject || LargeObjectCarrying || MediumObjectCarrying)
	{
		return;
	}

	if (SmallObjectArray.Num() > 0)
	{
		// If SmallObjectArray contains any objects, set ActorTointeract with to reference the most recently added object
		ActorToInteractWith = SmallObjectArray[SmallObjectArray.Num() - 1];
	}
	else if (MediumObjectArray.Num() > 0)
	{
		// Else if MediumObjectArray contains any objects, set ActorTointeract with to reference the most recently added object
		ActorToInteractWith = MediumObjectArray[MediumObjectArray.Num() - 1];
	}
	else if (LargeObjectArray.Num() > 0)
	{
		// Else if LargeObjectArray contains any objects, set ActorTointeract with to reference the most recently added object
		ActorToInteractWith = LargeObjectArray[LargeObjectArray.Num() - 1];
	}
	else if (SpecialisedObjectArray.Num() > 0)
	{
		// Else if SpecialisedObjectArray contains any objects, set ActorTointeract with to reference the most recently added object
		ActorToInteractWith = SpecialisedObjectArray[SpecialisedObjectArray.Num() - 1];
	}
	else
	{
		// Else if none of the arrays contain any objects, set ActorTointeract with to null
		ActorToInteractWith = nullptr;
	}
}

void UPlayerInteractionController::InteractKeyReleased()
{
	// If the interact key is released and the player is interacting with a LargeInteractableObject,
	// stop interacting with the object
	if (LargeObjectCarrying)
	{
		if (ULargeInteractableObject* LargeObject = LargeObjectCarrying->FindComponentByClass<ULargeInteractableObject>())
		{
			//bIsInteractingWithObject = false;
			LargeObjectCarrying = nullptr;
			LargeObject->OnEndInteract(this->GetOwner());
		}
	}
}

void UPlayerInteractionController::ReleaseMediumObject(/*UMediumInteractableObject* MediumObject, FVector NewPos*/)
{
	bIsInteractingWithObject = false;
	UMediumInteractableObject* MediumObject = MediumObjectCarryingTemp->FindComponentByClass<UMediumInteractableObject>();

	if (!MediumObject)
		return;

	MediumObject->OnEndInteract(this->GetOwner());
	MediumObjectCarryingTemp->SetActorLocation(NewMediumObjectPosition);
	MediumObjectCarryingTemp = nullptr;
}

void UPlayerInteractionController::PickUpSmallObject()
{
	if (SmallObjectArray.Num() > 0)
	{
		if (USmallInteractableObject* SmallObject = SmallObjectArray[SmallObjectArray.Num() - 1]->FindComponentByClass<USmallInteractableObject>())
		{
			SmallObject->OnBeginInteract(this->GetOwner());

			// Check if the player already owns an object by this name
			bool bIsKeyAlreadyOwned = false;
			KeySet.Add(SmallObject->ObjectName, &bIsKeyAlreadyOwned);
			if (bIsKeyAlreadyOwned)
			{
	#if WITH_EDITOR
				GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("ERROR: PlayerInteractionController::Object with this key already exists in KeySet."));
	#endif
				return;
			}

			SmallObjectArray.RemoveAt(SmallObjectArray.Num() - 1);

			// Destroy the object
			SmallObject->OnEndInteract(this->GetOwner());
		}
	}
}

void UPlayerInteractionController::PickUpMediumObject()
{
	UMediumInteractableObject* MediumObject = MediumObjectCarrying->FindComponentByClass<UMediumInteractableObject>();
	UStaticMeshComponent* MeshComp = MediumObjectCarrying->FindComponentByClass<UStaticMeshComponent>();
	if (MeshComp)
		PlacementGhost->UpdateStaticMesh(MeshComp->GetStaticMesh());
	else
		GEngine->AddOnScreenDebugMessage(0, 10.0f, FColor::Red, TEXT("PlayerInteractionController::Interact - Medium Object Mesh Component not found."));

	// Tell the object where to attach to
	MediumObject->PlayerSkeletalMesh = SkeletalMesh;
	MediumObject->PlayerSocketName = SocketName;
	MediumObject->PlayerSkeletalMeshRightVector = SkeletalMesh->GetRightVector();

	//FRotator NewRotation = SkeletalMesh->GetRightVector().Rotation();
	MediumObjectCarrying->SetActorRotation(SkeletalMesh->GetRightVector().Rotation());

	MediumObject->OnBeginInteract(this->GetOwner());
	// PlacementGhost->BeginPlacementMode(MediumObjectCarrying, SocketName);
}

void UPlayerInteractionController::ActivateObjectPlacementGhost()
{
	PlacementGhost->BeginPlacementMode(MediumObjectCarrying, SocketName);
}