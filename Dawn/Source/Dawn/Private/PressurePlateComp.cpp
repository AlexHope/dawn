// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "PressurePlateComp.h"

// THIS CLASS DOES NOTHING. Except for use in Interactble Placement Ghost.cpp See Line 97


// Sets default values for this component's properties
UPressurePlateComp::UPressurePlateComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UPressurePlateComp::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UPressurePlateComp::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
}

