// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "WallOcclusionReceiver.h"


// Sets default values for this component's properties
UWallOcclusionReceiver::UWallOcclusionReceiver()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWallOcclusionReceiver::BeginPlay()
{
	Super::BeginPlay();

	//Get the Static Mesh
	UStaticMeshComponent* StaticMesh = GetOwner()->FindComponentByClass<UStaticMeshComponent>();

	//Get Material
	UMaterialInterface* MaterialInterface = StaticMesh->GetMaterial(0);

	//Create Dynamic Material Instance
	MaterialInstanceDynamic = UMaterialInstanceDynamic::Create(MaterialInterface, nullptr, TEXT("MID_WallOcclusion"));

	//Set the default parameters
	MaterialInstanceDynamic->SetVectorParameterValue(TEXT("Location"), GetOwner()->GetRootComponent()->GetComponentLocation());
	MaterialInstanceDynamic->SetScalarParameterValue(TEXT("Radius"), Radius);
	MaterialInstanceDynamic->SetScalarParameterValue(TEXT("Hardness"), 1.0f);

	//Set the Material back onto the Mesh
	StaticMesh->SetMaterial(0, MaterialInstanceDynamic);
}


// Called every frame
void UWallOcclusionReceiver::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	//If our Radius is above 0, Reset it
	if (Radius > 0 && !bIsRevealingPlayer)
	{
		Radius = FMath::Lerp(Radius, 0.0f, DeltaTime * RadiusLerpScale);
		if (Radius < RadiusLerpThreshold)
		{
			Radius = 0;
		}
		MaterialInstanceDynamic->SetScalarParameterValue(TEXT("Radius"), Radius);
	}

	bIsRevealingPlayer = false;
}

void UWallOcclusionReceiver::RevealPlayer(FVector Location, float NewRadius)
{
	if (MaterialInstanceDynamic)
	{
		Radius = NewRadius;
		MaterialInstanceDynamic->SetVectorParameterValue(TEXT("Location"), Location);
		MaterialInstanceDynamic->SetScalarParameterValue(TEXT("Radius"), Radius);
		bIsRevealingPlayer = true;
	}
}

