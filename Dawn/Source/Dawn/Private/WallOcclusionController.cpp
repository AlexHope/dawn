// Fill out your copyright notice in the Description page of Project Settings.

#include "Dawn.h"
#include "WallOcclusionController.h"
#include "WallOcclusionReceiver.h"


// Sets default values for this component's properties
UWallOcclusionController::UWallOcclusionController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UWallOcclusionController::BeginPlay()
{
	Super::BeginPlay();

	//Get the player's camera manager
	PlayerCameraManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
	
	if (!PlayerCameraManager)
	{
			#if WITH_EDITOR
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("ERROR: No Player Camera Manager Found!"));
			}
			#endif
			return;
	}
}


// Called every frame
void UWallOcclusionController::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	TArray<FHitResult> HitResults;
	FVector StartTrace = PlayerCameraManager->GetCameraLocation();
	FVector EndTrace = GetOwner()->GetRootComponent()->GetComponentLocation();

	bool bCollisionHappened = GetWorld()->LineTraceMultiByChannel(HitResults, StartTrace, EndTrace, ECollisionChannel::ECC_Camera);
	//If our trace hit an object
	if (bCollisionHappened)
	{
		//Go through each object
		for(int i = 0; i < HitResults.Num(); i++)
		{
			//Check if Actor has the MaterialController Handler
			if (HitResults[i].Actor != nullptr && HitResults[i].Actor->FindComponentByClass<UWallOcclusionReceiver>())
			{
				UWallOcclusionReceiver* Receiver = HitResults[i].Actor->FindComponentByClass<UWallOcclusionReceiver>();
				Receiver->RevealPlayer(HitResults[i].ImpactPoint, Radius);
			}
		}
	}
}

