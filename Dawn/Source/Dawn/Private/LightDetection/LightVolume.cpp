#include "Dawn.h"
#include "Engine.h"
#include "LightVolume.h"


// Sets default values
ALightVolume::ALightVolume()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/* Create our root scene component and set it as root */
	RootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("ROOT"));
	RootComponent = RootSceneComponent;

	/* Create our light source and light volume colliders */
	SourceCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Light Source"));
	SourceCollider->SetupAttachment(RootComponent);
	SourceCollider->SetRelativeLocation(FVector(0.0f));
	SourceCollider->SetCollisionResponseToAllChannels(ECR_Ignore);
	SourceCollider->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);

	LightVolumeCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Light Volume"));
	LightVolumeCollider->SetupAttachment(RootComponent);
	LightVolumeCollider->SetRelativeLocation(FVector(0.0f));

	#if WITH_EDITOR
	SourceCollider->SetHiddenInGame(false);
	LightVolumeCollider->SetHiddenInGame(false);
	#endif
}

