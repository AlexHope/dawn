#include "Dawn.h"
#include "LightDetector.h"

// Sets default values for this component's properties
ULightDetector::ULightDetector()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	UpdateCount = 1;
	bRunOnEnter = true;
}

// Called when the game starts
void ULightDetector::BeginPlay()
{
	Super::BeginPlay();
	UCapsuleComponent* const CapsuleCollider = Cast<ACharacter>(GetOwner())->GetCapsuleComponent();
	CapsuleCollider->OnComponentBeginOverlap.AddDynamic(this, &ULightDetector::OnBeginOverlap);
	CapsuleCollider->OnComponentEndOverlap.AddDynamic(this, &ULightDetector::OnEndOverlap);

	// Setup Timer
	ResetTimer();

	/* Get character skeleton and bones */
	if (ACharacter* const MyCharacter = Cast<ACharacter>(GetOwner()))
	{
		MySkeleton = MyCharacter->GetMesh();
		if (MySkeleton)
		{
			Bones.Add(MySkeleton->GetBodyInstance("head"));				/* Head */
			Bones.Add(MySkeleton->GetBodyInstance("spine_02"));			/* Torso */
			Bones.Add(MySkeleton->GetBodyInstance("lowerarm_l"));		/* Elbow L*/
			Bones.Add(MySkeleton->GetBodyInstance("lowerarm_r"));		/* Elbow R */
			Bones.Add(MySkeleton->GetBodyInstance("calf_l"));			/* Knee L */
			Bones.Add(MySkeleton->GetBodyInstance("calf_r"));			/* Knee R */
			Bones.Add(MySkeleton->GetBodyInstance("foot_l"));			/* Foot L*/
			Bones.Add(MySkeleton->GetBodyInstance("foot_r"));			/* Foot R*/
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("Warning! No skeleton present on model"));
		}
	}

	if (Bones.Num() <= 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("Warning! No bones present"));
	}
}


// Called every frame
void ULightDetector::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Test timer
	Timer -= DeltaTime;
	if (Timer > 0)
		return;
	else
	{
		ResetTimer();
		RunLightTest();
	}
}

void ULightDetector::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	/* Check if we are colliding with a LightVolumeTrigger */
	if (UBoxComponent* const LightVolume = Cast<UBoxComponent>(OtherComp))
	{
		if (ALightVolume* const MyLightVolume = Cast<ALightVolume>(OtherActor))
		{
			LightVolumes.Add(MyLightVolume);
			OnLightVolumeEnter.Broadcast(MyLightVolume);
		}
			
		if (bRunOnEnter)
			RunLightTest();
	}
}

void ULightDetector::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	/* Check if we are leaving with a LightVolumeTrigger */
	if (UBoxComponent* const LightVolume = Cast<UBoxComponent>(OtherComp))
	{
		if (ALightVolume* const MyLightVolume = Cast<ALightVolume>(OtherActor))
		{
			LightVolumes.Remove(MyLightVolume);
			if (MyLightVolume->bInLightLastTest)
			{
				OnLightExit.Broadcast(MyLightVolume);
			}
			MyLightVolume->bInLightLastTest = false;
			OnLightVolumeExit.Broadcast(MyLightVolume);
		}
	}
}

void ULightDetector::RunLightTest() 
{
	if (!MySkeleton)
		return;

	bool bInLightGlobal = false;
	if (LightVolumes.Num() > 0 && Bones.Num())
	{
		TArray<FLightHitResult> HitResults;

		for (int i = 0; i < LightVolumes.Num(); i++)
		{
			bool bInLight = false;
			bool bHeadHit, bTorsoHit, bKneeHit, bFeetHit;
			bHeadHit = bTorsoHit = bKneeHit = bFeetHit = false;
			TArray<FBodyInstance> BonesHit;
			for (int j = 0; j < Bones.Num(); j++)
			{
				if (LightVolumes[i] && Bones[j])
				{
					FHitResult* HitResult = new FHitResult();
					FVector StartTrace = Bones[j]->GetUnrealWorldTransform().GetLocation();
					FVector EndTrace = LightVolumes[i]->SourceCollider->GetComponentLocation();
					bool bTrace = GetWorld()->LineTraceSingleByChannel(*HitResult, StartTrace, EndTrace, ECC_Visibility);
					if (bTrace)
					{
						#if WITH_EDITOR
						if (bDrawDebuggingLines)
						{
							DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor(200, 200, 0), false, 3.0f, 0, 1.0f);
							DrawDebugSphere(GetWorld(), StartTrace, 5.0f, 24, FColor(255, 0, 0), false, 3.0f, 0, 1.0f);
							DrawDebugSphere(GetWorld(), HitResult->ImpactPoint, 5.0f, 24, FColor(0, 255, 0), false, 3.0f, 0, 1.0f);
						}
						#endif

						if (HitResult->GetComponent() == LightVolumes[i]->SourceCollider)
						{
							bInLight = true;
							BonesHit.Add(*Bones[j]);
							
							if (Bones[j] == MySkeleton->GetBodyInstance("head"))
								bHeadHit = true;
							else if (Bones[j] == MySkeleton->GetBodyInstance("spine_02"))
								bTorsoHit = true;
							else if (Bones[j] == MySkeleton->GetBodyInstance("lowerarm_l") || Bones[j] == MySkeleton->GetBodyInstance("lowerarm_r"))
								bTorsoHit = true;
							else if (Bones[j] == MySkeleton->GetBodyInstance("calf_l") || Bones[j] == MySkeleton->GetBodyInstance("calf_r"))
								bKneeHit = true;
							else if (Bones[j] == MySkeleton->GetBodyInstance("foot_l") || Bones[j] == MySkeleton->GetBodyInstance("foot_r"))
								bFeetHit = true;
						}
					}
				}
			}

			if (bInLight)
			{
				FLightHitResult Results;
				Results.LightVolume = LightVolumes[i];
				Results.BonesHit = BonesHit;
				Results.bInLightHead = bHeadHit;
				Results.bInLightTorso = bTorsoHit;
				Results.bInLightKnee = bKneeHit;
				Results.bInLightFeet = bFeetHit;

				bInLightGlobal = bInLight;
				HitResults.Add(Results);
			}
			
			if (bInLight != LightVolumes[i]->bInLightLastTest)
			{
				if (bInLight)
				{
					OnLightEnter.Broadcast(LightVolumes[i]);
				}
				else
				{
					OnLightExit.Broadcast(LightVolumes[i]);
				}
				LightVolumes[i]->bInLightLastTest = bInLight;
			}
		}
		if (bInLightGlobal)
		{
			OnLightHitVerbose.Broadcast(HitResults);
		}
		
		bInLightLastTest = bInLightGlobal;
	}
	/*
	else
	{
		if (bInLightLastTest)
		{
			//OnLightExit.Broadcast(nullptr);
			bInLightLastTest = false;
		}
	}
	*/
}

void ULightDetector::ResetTimer()
{
	Timer = 1.0f / UpdateCount;
}
